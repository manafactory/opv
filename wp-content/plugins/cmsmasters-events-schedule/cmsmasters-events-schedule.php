<?php 
/*
Plugin Name: CMSMasters Events Schedule
Plugin URI: http://cmsmasters.net/
Description: CMSMasters Events Schedule created by <a href="http://cmsmasters.net/" title="CMSMasters">CMSMasters</a> team. Events Schedule plugin create custom post type that allow you to create events schedule for new <a href="http://themeforest.net/user/cmsmasters/portfolio" title="cmsmasters">cmsmasters</a> WordPress themes.
Version: 1.0.4
Author: cmsmasters
Author URI: http://cmsmasters.net/
*/

/*  Copyright 2016 CMSMasters (email : cmsmstrs@gmail.com). All Rights Reserved.
	
	This software is distributed exclusively as appendant 
	to Wordpress themes, created by CMSMasters studio and 
	should be used in strict compliance to the terms, 
	listed in the License Terms & Conditions included 
	in software archive.
	
	If your archive does not include this file, 
	you may find the license text by url 
	http://cmsmasters.net/files/license/cmsmasters-events-schedule/license.txt 
	or contact CMSMasters Studio at email 
	copyright.cmsmasters@gmail.com 
	about this.
	
	Please note, that any usage of this software, that 
	contradicts the license terms is a subject to legal pursue 
	and will result copyright reclaim and damage withdrawal.
*/


if (!defined('ABSPATH')) exit; // Exit if accessed directly


class Cmsmasters_Events_Schedule { 
	function __construct() { 
		define('CMSMASTERS_EVENTS_SCHEDULE_VERSION', '1.0.4');
		
		define('CMSMASTERS_EVENTS_SCHEDULE_FILE', __FILE__);
		
		$cmsmasters_active_theme = get_option('cmsmasters_active_theme') ? get_option('cmsmasters_active_theme') : '';
		
		define('CMSMASTERS_EVENTS_SCHEDULE_THEME_STYLE', (get_option('cmsmasters_' . $cmsmasters_active_theme . '_theme_style') ? get_option('cmsmasters_' . $cmsmasters_active_theme . '_theme_style') : ''));
		
		define('CMSMASTERS_EVENTS_SCHEDULE_NAME', plugin_basename(CMSMASTERS_EVENTS_SCHEDULE_FILE));
		
		define('CMSMASTERS_EVENTS_SCHEDULE_PATH', plugin_dir_path(CMSMASTERS_EVENTS_SCHEDULE_FILE));
		
		define('CMSMASTERS_EVENTS_SCHEDULE_URL', plugin_dir_url(CMSMASTERS_EVENTS_SCHEDULE_FILE));
		
		define('CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME', get_option('cmsmasters_active_theme'));
		
		define('CMSMASTERS_EVENTS_SCHEDULE_THEME_SHORTCODES_DIR', 'cmsmasters-events-schedule/cmsmasters-framework/theme-style' . CMSMASTERS_EVENTS_SCHEDULE_THEME_STYLE . '/cmsmasters-c-c/shortcodes');
		
		define('CMSMASTERS_EVENTS_SCHEDULE_THEME_TEMPLATES_DIR', 'cmsmasters-events-schedule/cmsmasters-framework/theme-style' . CMSMASTERS_EVENTS_SCHEDULE_THEME_STYLE . '/templates');
		
		
		require_once(CMSMASTERS_EVENTS_SCHEDULE_PATH . 'inc/cmsmasters-events-schedule-theme-settings-filter.php');
		
		require_once(CMSMASTERS_EVENTS_SCHEDULE_PATH . 'inc/cmsmasters-events-schedule-theme-options-filter.php');
 		
		
		require_once(CMSMASTERS_EVENTS_SCHEDULE_PATH . 'inc/events-schedule-posttype.php');
		
		require_once(CMSMASTERS_EVENTS_SCHEDULE_PATH . 'inc/cmsmasters-events-schedule-functions.php');
		
		require_once(CMSMASTERS_EVENTS_SCHEDULE_PATH . 'inc/cmsmasters-events-schedule-shortcodes.php');
		
		
		add_action('admin_enqueue_scripts', array($this, 'cmsmasters_events_schedule_admin_scripts'));
		
		add_action('wp_enqueue_scripts', array($this, 'cmsmasters_events_schedule_frontend_scripts'));
		
		add_action('admin_init', array($this, 'cmsmasters_events_schedule_compatibility'));
		
		
		register_activation_hook(CMSMASTERS_EVENTS_SCHEDULE_FILE, array($this, 'cmsmasters_events_schedule_activate'));
		
		register_deactivation_hook(CMSMASTERS_EVENTS_SCHEDULE_FILE, array($this, 'cmsmasters_events_schedule_deactivate'));
		
		
		// Load Plugin Local File
		load_plugin_textdomain('cmsmasters-events-schedule', false, dirname(plugin_basename(CMSMASTERS_EVENTS_SCHEDULE_FILE)) . '/languages/');
	}
	
	
	function cmsmasters_events_schedule_admin_scripts() {
		global $pagenow;
		
		
		if ( 
			$pagenow == 'post-new.php' || 
			($pagenow == 'post.php' && isset($_GET['post']) && get_post_type($_GET['post']) != 'attachment') 
		) {
			wp_enqueue_script('cmsmasters-events-schedule-c-c-shortcodes-extend', CMSMASTERS_EVENTS_SCHEDULE_URL . 'js/cmsmastersEventsSchedule-c-c-shortcodes-extend.js', array('cmsmasters_composer_shortcodes_js'), CMSMASTERS_EVENTS_SCHEDULE_VERSION, true);
			
			wp_localize_script('cmsmasters-events-schedule-c-c-shortcodes-extend', 'cmsmasters_events_schedule_shortcodes', array( 
				'events_schedule_title' =>							__('Events Schedule', 'cmsmasters-events-schedule'), 
				'events_schedule_field_events_schedule_title' =>	__('Events Schedule', 'cmsmasters-events-schedule'), 
				'events_schedule_field_events_schedule_descr' =>	__('Here you can add, edit, remove or sort events schedule', 'cmsmasters-events-schedule'), 
				'metadata_descr' =>									__('Choose events schedule metadata you want to be shown', 'cmsmasters-events-schedule'), 
				'choice_speaker' =>									__('Speaker', 'cmsmasters-events-schedule'), 
				'events_schedule_field_orderby_descr' =>			__('Choose what parameter your events schedule will be ordered by', 'cmsmasters-events-schedule'), 
				'event_schedule_title' =>							__('Event Schedule', 'cmsmasters-events-schedule'), 
				'event_schedule_field_date_title' =>				__('Dates', 'cmsmasters-events-schedule'), 
				'event_schedule_field_date_descr' =>				__('Show events schedule associated with certain dates', 'cmsmasters-events-schedule'), 
				'event_schedule_field_date_descr_note' =>			__('if empty - don\'t show events schedule', 'cmsmasters-events-schedule'), 
				'event_schedule_field_halls_title' =>				__('Halls', 'cmsmasters-events-schedule'), 
				'event_schedule_field_halls_descr' =>				__('Show events schedule associated with certain halls', 'cmsmasters-events-schedule'), 
				'event_schedule_field_halls_descr_note' =>			__('If you don\'t choose any event schedule halls, your events schedule will be not shown', 'cmsmasters-events-schedule'), 
				'event_schedule_field_notice_title' =>				__('Notice', 'cmsmasters-events-schedule'), 
				'event_schedule_field_notice_descr' =>				__('You can add notice for events schedule', 'cmsmasters-events-schedule') 
			));
		}
	}
	
	
	function cmsmasters_events_schedule_frontend_scripts() {
		wp_enqueue_script('cmsmasters-events-schedule-script', CMSMASTERS_EVENTS_SCHEDULE_URL . 'js/cmsmastersEventsSchedule-script.js', array('jquery'), CMSMASTERS_EVENTS_SCHEDULE_VERSION, true);
	}
	
	
	public function cmsmasters_events_schedule_activate() {
		$this->cmsmasters_events_schedule_activation_compatibility();
		
		
		if (get_option('cmsmasters_active_events_schedule') != CMSMASTERS_EVENTS_SCHEDULE_VERSION) {
			add_option('cmsmasters_active_events_schedule', CMSMASTERS_EVENTS_SCHEDULE_VERSION, '', 'yes');
		}
		
		
		flush_rewrite_rules();
	}
	
	
	public function cmsmasters_events_schedule_deactivate() {
		flush_rewrite_rules();
	}
	
	
	public function cmsmasters_events_schedule_activation_compatibility() {
		if ( 
			!defined('CMSMASTERS_EVENTS_SCHEDULE') || 
			(defined('CMSMASTERS_EVENTS_SCHEDULE') && !CMSMASTERS_EVENTS_SCHEDULE) 
		) {
			deactivate_plugins(CMSMASTERS_EVENTS_SCHEDULE_NAME);
			
			
			wp_die( 
				__("Your theme doesn't support CMSMasters Events Schedule plugin. Please use appropriate CMSMasters theme.", 'cmsmasters-events-schedule'), 
				__("Error!", 'cmsmasters-events-schedule'), 
				array( 
					'back_link' => 	true 
				) 
			);
		}
	}
	
	
	public function cmsmasters_events_schedule_compatibility() {
		if ( 
			!defined('CMSMASTERS_EVENTS_SCHEDULE') || 
			(defined('CMSMASTERS_EVENTS_SCHEDULE') && !CMSMASTERS_EVENTS_SCHEDULE) 
		) {
			if (is_plugin_active(CMSMASTERS_EVENTS_SCHEDULE_NAME)) {
				deactivate_plugins(CMSMASTERS_EVENTS_SCHEDULE_NAME);
				
				
				add_action('admin_notices', array($this, 'cmsmasters_events_schedule_compatibility_warning'));
			}
		}
	}
	
	
	public function cmsmasters_events_schedule_compatibility_warning() {
		echo "<div class=\"notice notice-warning is-dismissible\">
			<p><strong>" . __("CMSMasters Events Schedule plugin was deactivated, because your theme doesn't support it. Please use appropriate CMSMasters theme.", 'cmsmasters-events-schedule') . "</strong></p>
		</div>";
	}
}


new Cmsmasters_Events_Schedule();

