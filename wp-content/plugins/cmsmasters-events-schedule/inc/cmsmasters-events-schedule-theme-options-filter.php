<?php 
/**
 * @package 	WordPress Plugin
 * @subpackage 	CMSMasters Events Schedule
 * @version		1.0.0
 * 
 * CMSMasters Events Schedule Options Filter
 * Created by CMSMasters
 * 
 */


function cmsmasters_events_shedule_meta_fields($custom_all_meta_fields) {
	$custom_all_meta_fields_new = array();
	
	
	if (
		(isset($_GET['post_type']) && $_GET['post_type'] == 'event-schedule') || 
		(isset($_POST['post_type']) && $_POST['post_type'] == 'event-schedule') || 
		(isset($_GET['post']) && get_post_type($_GET['post']) == 'event-schedule') 
	) {
		$cmsmasters_option = array();
		
		$cmsmasters_option_name = 'cmsmasters_options_' . CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_single_event_schedule';
		
		
		if (get_option($cmsmasters_option_name) != false) {
			$option = get_option($cmsmasters_option_name);
			
			$cmsmasters_option = array_merge($cmsmasters_option, $option);
		}
		
		
		$cmsmasters_global_event_schedule_layout = (isset($cmsmasters_option[CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_layout']) && $cmsmasters_option[CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_layout'] !== '') ? $cmsmasters_option[CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_layout'] : 'r_sidebar';
		
		$cmsmasters_global_event_schedule_title = (isset($cmsmasters_option[CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_title']) && $cmsmasters_option[CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_title'] !== '') ? (($cmsmasters_option[CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_title'] == 1) ? 'true' : 'false') : 'true';
		
		$cmsmasters_global_event_schedule_share_box = (isset($cmsmasters_option[CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_share_box']) && $cmsmasters_option[CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_share_box'] !== '') ? (($cmsmasters_option[CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_share_box'] == 1) ? 'true' : 'false') : 'true';
		
		
		foreach ($custom_all_meta_fields as $custom_all_meta_field) {
			if ($custom_all_meta_field['id'] == 'cmsmasters_other_tabs') {
				$custom_all_meta_field['std'] = 'cmsmasters_event_schedule';
				
				
				$tabs_array = array();
				
				$tabs_array['cmsmasters_event_schedule'] = array( 
					'label' => esc_html__('Events Schedule', 'cmsmasters-events-schedule'), 
					'value' => 'cmsmasters_event_schedule' 
				);
				
				
				foreach ($custom_all_meta_field['options'] as $key => $val) {
					$tabs_array[$key] = $val;
				}
				
				
				$custom_all_meta_field['options'] = $tabs_array;
				
				
				$custom_all_meta_fields_new[] = $custom_all_meta_field;
			} elseif (
				$custom_all_meta_field['id'] == 'cmsmasters_layout' && 
				$custom_all_meta_field['type'] == 'tab_start'
			) {
				$custom_all_meta_field['std'] = '';
				
				
				$custom_all_meta_fields_new[] = array( 
					'id'	=> 'cmsmasters_event_schedule', 
					'type'	=> 'tab_start', 
					'std'	=> 'true'
				);
				
				$custom_all_meta_fields_new[] = array( 
					'label'	=> esc_html__('Event Schedule Title', 'cmsmasters-events-schedule'), 
					'desc'	=> esc_html__('Show', 'cmsmasters-events-schedule'), 
					'id'	=> 'cmsmasters_event_schedule_title', 
					'type'	=> 'checkbox', 
					'hide'	=> '', 
					'std'	=> $cmsmasters_global_event_schedule_title 
				);
				
				$custom_all_meta_fields_new[] = array( 
					'label'	=> esc_html__('Event Schedule Time', 'cmsmasters-events-schedule'), 
					'desc'	=> '', 
					'id'	=> 'cmsmasters_event_schedule_time', 
					'type'	=> 'text_long', 
					'hide'	=> '', 
					'std'	=> '' 
				);
				
				$custom_all_meta_fields_new[] = array( 
					'label'	=> esc_html__('Speaker', 'cmsmasters-events-schedule'), 
					'desc'	=> '', 
					'id'	=> 'cmsmasters_event_schedule_speaker', 
					'type'	=> 'text_long', 
					'hide'	=> '', 
					'std'	=> '' 
				);
				
				$custom_all_meta_fields_new[] = array( 
					'label'	=> esc_html__('Speaker Heading', 'cmsmasters-events-schedule'), 
					'desc'	=> '', 
					'id'	=> 'cmsmasters_event_schedule_speaker_heading', 
					'type'	=> 'text_long', 
					'hide'	=> '', 
					'std'	=> '' 
				);
				
				$custom_all_meta_fields_new[] = array( 
					'label'	=> esc_html__('Sharing Box', 'cmsmasters-events-schedule'), 
					'desc'	=> esc_html__('Show', 'cmsmasters-events-schedule'), 
					'id'	=> 'cmsmasters_event_schedule_sharing_box', 
					'type'	=> 'checkbox', 
					'hide'	=> '', 
					'std'	=> $cmsmasters_global_event_schedule_share_box 
				);
				
				$custom_all_meta_fields_new[] = array( 
					'id'	=> 'cmsmasters_event_schedule', 
					'type'	=> 'tab_finish'
				);
				
				
				$custom_all_meta_fields_new[] = $custom_all_meta_field;
			} elseif (
				$custom_all_meta_field['id'] == 'cmsmasters_layout' && 
				$custom_all_meta_field['type'] != 'tab_start' && 
				$custom_all_meta_field['type'] != 'tab_finish'
			) {
				$custom_all_meta_field['std'] = $cmsmasters_global_event_schedule_layout;
				
				
				$custom_all_meta_fields_new[] = $custom_all_meta_field;
			} else {
				$custom_all_meta_fields_new[] = $custom_all_meta_field;
			}
		}
	} else {
		$custom_all_meta_fields_new = $custom_all_meta_fields;
	}
	
	
	return $custom_all_meta_fields_new;
}

add_filter('get_custom_all_meta_fields_filter', 'cmsmasters_events_shedule_meta_fields');

