<?php 
/**
 * @package 	WordPress Plugin
 * @subpackage 	CMSMasters Events Schedule
 * @version		1.0.4
 * 
 * CMSMasters Events Schedule Settings Filter
 * Created by CMSMasters
 * 
 */


/* Single Settings */
// Settings Names
function cmsmasters_events_shedule_option_name($cmsmasters_option_name, $tab) {
	if ($tab == 'event_schedule') {
		$cmsmasters_option_name = 'cmsmasters_options_' . CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_single_event_schedule';
	}
	
	
	return $cmsmasters_option_name;
}

add_filter('cmsmasters_option_name_filter', 'cmsmasters_events_shedule_option_name', 10, 2);


// Add Settings
function cmsmasters_events_shedule_add_global_options($cmsmasters_option_names) {
	$cmsmasters_option_names[] = array( 
		'cmsmasters_options_' . CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_single_event_schedule', 
		cmsmasters_events_shedule_options_single_fields('', 'event_schedule') 
	);
	
	
	return $cmsmasters_option_names;
}

add_filter('cmsmasters_add_global_options_filter', 'cmsmasters_events_shedule_add_global_options');


// Get Settings
function cmsmasters_events_shedule_get_global_options($cmsmasters_option_names) {
	array_push($cmsmasters_option_names, 'cmsmasters_options_' . CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_single_event_schedule');
	
	
	return $cmsmasters_option_names;
}

add_filter('cmsmasters_get_global_options_filter', 'cmsmasters_events_shedule_get_global_options');
add_filter('cmsmasters_settings_export_filter', 'cmsmasters_events_shedule_get_global_options');


// Single Posts Settings
function cmsmasters_events_shedule_options_single_tabs($tabs) {
	$tabs['event_schedule'] = esc_html__('Event Schedule', 'cmsmasters-events-schedule');
	
	
	return $tabs;
}

add_filter('cmsmasters_options_single_tabs_filter', 'cmsmasters_events_shedule_options_single_tabs');


function cmsmasters_events_shedule_options_single_sections($sections, $tab) {
	if ($tab == 'event_schedule') {
		$sections = array();
		
		$sections['event_schedule_section'] = esc_attr__('Event Schedule Options', 'cmsmasters-events-schedule');
	}
	
	
	return $sections;
}

add_filter('cmsmasters_options_single_sections_filter', 'cmsmasters_events_shedule_options_single_sections', 10, 2);


function cmsmasters_events_shedule_options_single_fields($options, $tab) {
	if (!is_array($options)) {
		$options = array();
	}
	
	
	if ($tab == 'event_schedule') {
		$options[] = array( 
			'section' => 'event_schedule_section', 
			'id' => CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_layout', 
			'title' => esc_html__('Event Schedule Layout Type', 'cmsmasters-events-schedule'), 
			'desc' => '', 
			'type' => 'radio_img', 
			'std' => 'r_sidebar', 
			'choices' => array( 
				esc_html__('Right Sidebar', 'cmsmasters-events-schedule') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'cmsmasters-events-schedule') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'cmsmasters-events-schedule') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'event_schedule_section', 
			'id' => CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_title', 
			'title' => esc_html__('Event Schedule Title', 'cmsmasters-events-schedule'), 
			'desc' => esc_html__('show', 'cmsmasters-events-schedule'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'event_schedule_section', 
			'id' => CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_nav_box', 
			'title' => esc_html__('Event Schedule Navigation Box', 'cmsmasters-events-schedule'), 
			'desc' => esc_html__('show', 'cmsmasters-events-schedule'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'event_schedule_section', 
			'id' => CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_share_box', 
			'title' => esc_html__('Sharing Box', 'cmsmasters-events-schedule'), 
			'desc' => esc_html__('show', 'cmsmasters-events-schedule'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'event_schedule_section', 
			'id' => CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_post_slug', 
			'title' => esc_html__('Event Schedule Slug', 'cmsmasters-events-schedule'), 
			'desc' => esc_html__('Enter a page slug that should be used for your events schedule single item', 'cmsmasters-events-schedule'), 
			'type' => 'text', 
			'std' => 'event-schedule', 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'event_schedule_section', 
			'id' => CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_es_date_slug', 
			'title' => esc_html__('Event Schedule Dates Slug', 'cmsmasters-events-schedule'), 
			'desc' => esc_html__('Enter page slug that should be used on events schedule dates archive page', 'cmsmasters-events-schedule'), 
			'type' => 'text', 
			'std' => 'es-date', 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'event_schedule_section', 
			'id' => CMSMASTERS_EVENTS_SCHEDULE_ACTIVE_THEME . '_event_schedule_es_hall_slug', 
			'title' => esc_html__('Event Schedule Halls Slug', 'cmsmasters-events-schedule'), 
			'desc' => esc_html__('Enter page slug that should be used on events schedule tags archive page', 'cmsmasters-events-schedule'), 
			'type' => 'text', 
			'std' => 'es-hall', 
			'class' => '' 
		);
	}
	
	
	return $options;
}

add_filter('cmsmasters_options_single_fields_filter', 'cmsmasters_events_shedule_options_single_fields', 10, 2);

