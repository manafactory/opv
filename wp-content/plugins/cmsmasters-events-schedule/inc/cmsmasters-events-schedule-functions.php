<?php 
/**
 * @package 	WordPress Plugin
 * @subpackage 	CMSMasters Events Schedule
 * @version		1.0.0
 * 
 * CMSMasters Events Schedule Composer Functions
 * Created by CMSMasters
 * 
 */


// Shortcodes Init
function cmsmasters_events_schedule_shortcodes_init() {
	global $pagenow;
	
	
	if ( 
		is_admin() && 
		$pagenow == 'post-new.php' || 
		($pagenow == 'post.php' && isset($_GET['post']) && get_post_type($_GET['post']) != 'attachment') 
	) {
		if (wp_script_is('cmsmasters_content_composer_js', 'queue') && wp_script_is('cmsmasters_composer_lightbox_js', 'queue')) {
			cmsmasters_events_schedule_es_dates();
			
			cmsmasters_events_schedule_es_halls();
		}
	}
}

add_action('admin_footer', 'cmsmasters_events_schedule_shortcodes_init');


// Events Schedule Dates
function cmsmasters_events_schedule_es_dates() {
	$dates = get_terms('es-date', array( 
		'hide_empty' => 0 
	));
	
	
	$out = "\n" . '<script type="text/javascript"> ' . "\n" . 
	'/* <![CDATA[ */' . "\n\t" . 
		'function cmsmasters_events_schedule_es_dates() { ' . "\n\t\t" . 
			'return { ' . "\n";
			
	$out .= "\t\t\t\"\" : \"" . esc_html__('None', 'cmsmasters-events-schedule') . "\", \n";
	
	
	if (!empty($dates) && !is_wp_error($dates)) {
		foreach ($dates as $date) {
			$out .= "\t\t\t\"" . esc_html($date->name) . "\" : \"" . esc_html($date->name) . "\", \n";
		}
		
		
		$out = substr($out, 0, -3);
	}
	
	
	$out .= "\n\t\t" . '}; ' . "\n\t" . 
		'} ' . "\n" . 
	'/* ]]> */' . "\n" . 
	'</script>' . "\n\n";
	
	
	echo $out;
}


// Events Schedule Halls
function cmsmasters_events_schedule_es_halls() {
	$halls = get_terms('es-hall', array( 
		'hide_empty' => 0 
	));
	
	
	$out = "\n" . '<script type="text/javascript"> ' . "\n" . 
	'/* <![CDATA[ */' . "\n\t" . 
		'function cmsmasters_events_schedule_es_halls() { ' . "\n\t\t" . 
			'return { ' . "\n";
	
	
	if (!empty($halls) && !is_wp_error($halls)) {
		foreach ($halls as $hall) {
			$out .= "\t\t\t\"" . esc_html($hall->name) . "\" : \"" . esc_html($hall->name) . "\", \n";
		}
		
		
		$out = substr($out, 0, -3);
	}
	
	
	$out .= "\n\t\t" . '}; ' . "\n\t" . 
		'} ' . "\n" . 
	'/* ]]> */' . "\n" . 
	'</script>' . "\n\n";
	
	
	echo $out;
}


function cmsmasters_events_schedule_ob_load_template($template_name, $args = array()) {
	if (locate_template($template_name)) {
		$template = locate_template($template_name);
		
		
		if (is_array($args) && !empty($args)) {
			extract($args);
		}
		
		
		ob_start();
		
		
		include($template);
		
		
		$out = ob_get_contents();
		
		
		ob_end_clean();
		
		
		return $out;
	}
}

