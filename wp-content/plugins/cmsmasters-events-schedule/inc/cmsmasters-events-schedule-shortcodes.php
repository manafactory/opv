<?php 
/**
 * @package 	WordPress Plugin
 * @subpackage 	CMSMasters Events Schedule
 * @version		1.0.3
 * 
 * CMSMasters Events Schedule Shortcodes Functions
 * Created by CMSMasters
 * 
 */


class Cmsmasters_Events_Schedule_Shortcodes {

public function __construct() {
	add_shortcode('cmsmasters_schedule_events', array($this, 'cmsmasters_events_schedule'));
	
	add_shortcode('cmsmasters_schedule_event', array($this, 'cmsmasters_event_schedule'));
}



/**
 * Events Schedule
 */
public $events_schedule_atts;

public function cmsmasters_events_schedule($atts, $content = null) { 
    $new_atts = apply_filters('cmsmasters_events_schedule_atts_filter', array( 
		'shortcode_id' => 		'', 
		'metadata' => 			'', 
		'orderby' => 			'date', 
		'order' => 				'DESC', 
		'classes' => 			'' 
    ) );
	
	
	$shortcode_name = 'events-schedule';
	
	$shortcode_path = CMSMASTERS_EVENTS_SCHEDULE_THEME_SHORTCODES_DIR . '/cmsmasters-' . $shortcode_name . '.php';
	
	
	if (locate_template($shortcode_path)) {
		ob_start();
		
		
		include(locate_template($shortcode_path));
		
		
		$template_out = ob_get_contents();
		
		
		ob_end_clean();
		
		
		return $template_out;
	}
	
	
	extract(shortcode_atts($new_atts, $atts));
	
	
	$unique_id = $shortcode_id;
	
	
	$this->events_schedule_atts = array(
		'date' => 						array(), 
		'cmsmasters_es_metadata' => 	$metadata, 
		'orderby' => 					$orderby, 
		'order' => 						$order, 
		'event_schedule_out' => 		'' 
	);
	
	
	$cmsmasters_event_schedule = do_shortcode($content);
	
	
	$out = '';
	
	$date_empty = '';
	
	
	foreach ($this->events_schedule_atts['date'] as $date) {
		$date_empty .= $date;
	}
	
	
	if ($date_empty == '') {
		$out .= '<div id="cmsmasters_events_schedule_' . $unique_id . '" class="cmsmasters_events_schedule_empty">' . 
			esc_html__('Please, choose date', 'cmsmasters-events-schedule') . 
		'</div>';
		
		
		return $out;
	}
	
	
	$out .= '<div id="cmsmasters_events_schedule_' . $unique_id . '" class="cmsmasters_events_schedule cmsmasters_events_schedule_tabs' . 
		(($classes != '') ? ' ' . $classes : '') . 
		'"' . 
	'>' . 
		'<ul class="cmsmasters_events_schedule_list_date cmsmasters_events_schedule_tabs_list">' . 
			$this->events_schedule_atts['event_schedule_out'] . 
		'</ul>' . 
		'<div class="cmsmasters_events_schedule_wrap_date cmsmasters_events_schedule_tabs_items">' . 
			$cmsmasters_event_schedule . 
		'</div>' . 
	'</div>';
	
	
	return $out;
}


/**
 * Event Schedule
 */
public function cmsmasters_event_schedule($atts, $content = null) {
    $new_atts = apply_filters('cmsmasters_event_schedule_atts_filter', array( 
		'shortcode_id' => 	'', 
		'date' => 			'', 
		'halls' => 			'', 
		'classes' => 		'' 
    ) );
	
	
	$shortcode_name = 'event-schedule';
	
	$shortcode_path = CMSMASTERS_EVENTS_SCHEDULE_THEME_SHORTCODES_DIR . '/cmsmasters-' . $shortcode_name . '.php';
	
	
	if (locate_template($shortcode_path)) {
		ob_start();
		
		
		include(locate_template($shortcode_path));
		
		
		$template_out = ob_get_contents();
		
		
		ob_end_clean();
		
		
		return $template_out;
	}
	
	
	extract(shortcode_atts($new_atts, $atts));
	
	
	$this->events_schedule_atts['date'][] = esc_html($date);
	
	
	if ($date == '') {
		return '';
	}
	
	
	$this->events_schedule_atts['event_schedule_out'] .= '<li class="cmsmasters_event_schedule_list_item_date">' . 
		'<a href="#">' . 
			'<span>' . esc_html($date) . '</span>' . 
		'</a>' . 
	'</li>';
	
	
	$out = '<div class="cmsmasters_events_schedule_date_tab' . 
		(($classes != '') ? ' ' . $classes : '') . 
		(($halls == '') ? ' ' . 'no_halls' : '') . 
	'">';
	
	
	if ($halls != '') {
		$halls_array = explode(',', $halls);
		
		
		$out .= '<ul class="cmsmasters_events_schedule_list_hall cmsmasters_events_schedule_tabs_list">';
			
			
			foreach ($halls_array as $hall) {
				$out .= '<li class="cmsmasters_events_schedule_list_item_hall">' . 
					'<a href="#">' . 
						'<span>' . esc_html($hall) . '</span>' . 
					'</a>' . 
				'</li>';
			}
			
			
		$out .= '</ul>';
		
		
		$out .= '<div class="cmsmasters_events_schedule_wrap_hall cmsmasters_events_schedule_tabs_items">';
			
			
			foreach ($halls_array as $hall) {
				$args = array( 
					'post_type' => 				'event-schedule', 
					'orderby' => 				$this->events_schedule_atts['orderby'], 
					'order' => 					$this->events_schedule_atts['order'], 
					'ignore_sticky_posts' => 	true, 
					'posts_per_page' => 		'1000', 
					'tax_query' => array( 
						'relation' => 'AND', 
						array( 
							'taxonomy' => 	'es-date', 
							'field' => 		'name', 
							'terms' => 		$date 
						), 
						array( 
							'taxonomy' => 	'es-hall', 
							'field' => 		'name', 
							'terms' => 		$hall 
						)
					)
				);
				
				
				$event_schedule_query = new WP_Query($args);
				
				
				$out .= '<div class="cmsmasters_events_schedule_wrap' . (($event_schedule_query->have_posts()) ? ' ' . 'have_posts' : '') . '">' . 
					esc_html($content) . 
					'<div class="cmsmasters_event_schedule_inner">';
					
					
					if ($event_schedule_query->have_posts()) {
						while ($event_schedule_query->have_posts()) : $event_schedule_query->the_post();
							
							$out .= cmsmasters_events_schedule_ob_load_template('cmsmasters-events-schedule/cmsmasters-framework/theme-style' . CMSMASTERS_EVENTS_SCHEDULE_THEME_STYLE . '/postType/standard.php', $this->events_schedule_atts);
							
						endwhile;
					} else {
						$out .= esc_html__('Not found posts', 'cmsmasters-events-schedule');
					}
					
					
					$out .= '</div>' . 
				'</div>';
			}
			
			
		$out .= '</div>';
	} else {
		$out .= esc_html__('Please, choose hall', 'cmsmasters-events-schedule');
	}
	
	
	$out .= '</div>';
	
	
	wp_reset_query();
	
	
	return $out;
}

}

new Cmsmasters_Events_Schedule_Shortcodes();

