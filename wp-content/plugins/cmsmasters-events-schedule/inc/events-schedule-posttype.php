<?php 
/**
 * @package 	WordPress Plugin
 * @subpackage 	CMSMasters Events Schedule
 * @version		1.0.0
 * 
 * CMSMasters Events Schedule Post Type
 * Created by CMSMasters
 * 
 */


class Cmsmasters_Events_Schedule_Post_Type {
	public function __construct() {
		$current_theme = get_option('template');
		
		$event_schedule_post_settings_array = get_option('cmsmasters_options_' . $current_theme . '_single_event_schedule');
		
		$event_schedule_post_slug = $event_schedule_post_settings_array[$current_theme . '_event_schedule_post_slug'];
		
		$event_schedule_es_date_slug = $event_schedule_post_settings_array[$current_theme . '_event_schedule_es_date_slug'];
		
		$event_schedule_es_hall_slug = $event_schedule_post_settings_array[$current_theme . '_event_schedule_es_hall_slug'];
		
		
		$event_schedule_labels = apply_filters('cmsmasters_event_schedule_labels_filter', array( 
			'name' => 					__('Events Schedule', 'cmsmasters-events-schedule'), 
			'singular_name' => 			__('Events Schedule', 'cmsmasters-events-schedule'), 
			'menu_name' => 				__('Events Schedule', 'cmsmasters-events-schedule'), 
			'all_items' => 				__('All Events Schedule', 'cmsmasters-events-schedule'), 
			'add_new' => 				__('Add New', 'cmsmasters-events-schedule'), 
			'add_new_item' => 			__('Add New Event Schedule', 'cmsmasters-events-schedule'), 
			'edit_item' => 				__('Edit Event Schedule', 'cmsmasters-events-schedule'), 
			'new_item' => 				__('New Event Schedule', 'cmsmasters-events-schedule'), 
			'view_item' => 				__('View Event Schedule', 'cmsmasters-events-schedule'), 
			'search_items' => 			__('Search Events Schedule', 'cmsmasters-events-schedule'), 
			'not_found' => 				__('No Events Schedule found', 'cmsmasters-events-schedule'), 
			'not_found_in_trash' => 	__('No Events Schedule found in Trash', 'cmsmasters-events-schedule') 
		) );
		
		
		$event_schedule_args = array( 
			'labels' => 			$event_schedule_labels, 
			'query_var' => 			'event_schedule', 
			'capability_type' => 	'post', 
			'menu_position' => 		50, 
			'menu_icon' => 			'dashicons-edit', 
			'public' => 			true, 
			'show_ui' => 			true, 
			'hierarchical' => 		false, 
			'has_archive' => 		true, 
			'supports' => array( 
				'title', 
				'editor', 
				'thumbnail', 
				'excerpt', 
				'trackbacks', 
				'custom-fields', 
				'comments', 
				'revisions', 
				'page-attributes' 
			), 
			'rewrite' => array( 
				'slug' => 			(isset($event_schedule_post_slug) && $event_schedule_post_slug != '') ? $event_schedule_post_slug : 'event-schedule', 
				'with_front' => 	true 
			) 
		);
		
		
		register_post_type('event-schedule', $event_schedule_args);
		
		
		$es_date_labels = apply_filters('cmsmasters_es_date_labels_filter', array( 
			'name' => 					__('Event Schedule Dates', 'cmsmasters-events-schedule'), 
			'singular_name' => 			__('Event Schedule Day', 'cmsmasters-events-schedule') 
		) );
		
		
		$es_date_args = array (
			'hierarchical' => 		false, 
			'labels' => 			$es_date_labels, 
			'rewrite' => array( 
				'slug' => 			(isset($event_schedule_es_date_slug) && $event_schedule_es_date_slug != '') ? $event_schedule_es_date_slug : 'es-date', 
				'with_front' => 	true 
			) 
		);
		
		register_taxonomy('es-date', array('event-schedule'), $es_date_args);
		
		
		$es_hall_labels = apply_filters('cmsmasters_es_hall_labels_filter', array( 
			'name' => 					__('Event Schedule Halls', 'cmsmasters-events-schedule'), 
			'singular_name' => 			__('Event Schedule Hall', 'cmsmasters-events-schedule') 
		) );
		
		
		$es_hall_args = array (
			'hierarchical' => 		false, 
			'labels' => 			$es_hall_labels, 
			'rewrite' => array( 
				'slug' => 			(isset($event_schedule_es_hall_slug) && $event_schedule_es_hall_slug != '') ? $event_schedule_es_hall_slug : 'es-hall', 
				'with_front' => 	true 
			) 
		);
		
		
		register_taxonomy('es-hall', array('event-schedule'), $es_hall_args);
	}
}


function cmsmasters_events_schedule_init() {
	global $es;
	
	$es = new Cmsmasters_Events_Schedule_Post_Type();
}

add_action('init', 'cmsmasters_events_schedule_init');

