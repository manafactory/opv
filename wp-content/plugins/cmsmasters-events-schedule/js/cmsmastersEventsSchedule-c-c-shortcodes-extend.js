/**
 * @package 	WordPress Plugin
 * @subpackage 	CMSMasters Events Schedule
 * @version		1.0.2
 * 
 * CMSMasters Events Schedule Content Composer Schortcodes Extend
 * Created by CMSMasters
 * 
 */


/**
 * Events Schedule
 */
cmsmastersShortcodes.cmsmasters_schedule_events = { 
	title : 	cmsmasters_events_schedule_shortcodes.events_schedule_title, 
	icon : 		'admin-icon-edit', 
	pair : 		true, 
	content : 	'events_schedule', 
	visual : 	false, 
	multiple : 	true, 
	def : 		'[cmsmasters_schedule_event][/cmsmasters_schedule_event]', 
	fields : { 
		// Shortcode ID
		shortcode_id : { 
			type : 		'hidden', 
			title : 	'', 
			descr : 	'', 
			def : 		'', 
			required : 	true, 
			width : 	'full' 
		}, 
		// Events Schedule
		events_schedule : { 
			type : 		'multiple', 
			title : 	cmsmasters_events_schedule_shortcodes.events_schedule_field_events_schedule_title, 
			descr : 	cmsmasters_events_schedule_shortcodes.events_schedule_field_events_schedule_descr, 
			def : 		'[cmsmasters_schedule_event][/cmsmasters_schedule_event]',
			required : 	true, 
			width : 	'half' 
		}, 
		// Metadata
		metadata : { 
			type : 		'checkbox', 
			title : 	cmsmasters_shortcodes.metadata, 
			descr : 	cmsmasters_events_schedule_shortcodes.metadata_descr, 
			def : 		'excerpt,speaker', 
			required : 	true, 
			width : 	'half', 
			choises : { 
						'excerpt' : 		cmsmasters_shortcodes.choice_excerpt, 
						'speaker' : 		cmsmasters_events_schedule_shortcodes.choice_speaker 
			} 
		}, 
		// Order By
		orderby : { 
			type : 		'select', 
			title : 	cmsmasters_shortcodes.orderby_title, 
			descr : 	cmsmasters_events_schedule_shortcodes.events_schedule_field_orderby_descr, 
			def : 		'date', 
			required : 	true, 
			width : 	'half', 
			choises : { 
						'date' : 		cmsmasters_shortcodes.choice_date, 
						'name' : 		cmsmasters_shortcodes.name, 
						'id' : 			cmsmasters_shortcodes.choice_id, 
						'menu_order' : 	cmsmasters_shortcodes.choice_menu 
			} 
		}, 
		// Order
		order : { 
			type : 		'radio', 
			title : 	cmsmasters_shortcodes.order_title, 
			descr : 	cmsmasters_shortcodes.order_descr, 
			def : 		'DESC', 
			required : 	true, 
			width : 	'half', 
			choises : { 
						'ASC' : 	cmsmasters_shortcodes.choice_asc, 
						'DESC' : 	cmsmasters_shortcodes.choice_desc 
			} 
		}, 
		// Additional Classes
		classes : { 
			type : 		'input', 
			title : 	cmsmasters_shortcodes.classes_title, 
			descr : 	cmsmasters_shortcodes.classes_descr, 
			def : 		'', 
			required : 	false, 
			width : 	'half' 
		} 
	} 
};



/**
 * Event Schedule
 */
cmsmastersMultipleShortcodes.cmsmasters_schedule_event = { 
	title : 	cmsmasters_events_schedule_shortcodes.event_schedule_title, 
	pair : 		true, 
	content : 	'notice', 
	visual : 	'<span class="cmsmasters_multiple_text">{{ data.date }}</span>', 
	def : 		'', 
	fields : { 
		// Shortcode ID
		shortcode_id : { 
			type : 		'hidden', 
			title : 	'', 
			descr : 	'', 
			def : 		'', 
			required : 	true, 
			width : 	'full' 
		}, 
		// Date
		date : { 
			type : 		'select', 
			title : 	cmsmasters_events_schedule_shortcodes.event_schedule_field_date_title, 
			descr : 	cmsmasters_events_schedule_shortcodes.event_schedule_field_date_descr + "<br /><span>" + cmsmasters_shortcodes.note + ' ' + cmsmasters_events_schedule_shortcodes.event_schedule_field_date_descr_note + "</span>", 
			def : 		'', 
			required : 	true, 
			width : 	'half', 
			choises : 	cmsmasters_events_schedule_es_dates() 
		}, 
		// Halls
		halls : { 
			type : 		'select_multiple', 
			title : 	cmsmasters_events_schedule_shortcodes.event_schedule_field_halls_title, 
			descr : 	cmsmasters_events_schedule_shortcodes.event_schedule_field_halls_descr + "<br /><span>" + cmsmasters_shortcodes.note + ' ' + cmsmasters_events_schedule_shortcodes.event_schedule_field_halls_descr_note + "</span>", 
			def : 		'', 
			required : 	true, 
			width : 	'half', 
			choises : 	cmsmasters_events_schedule_es_halls() 
		}, 
		// Notice
		notice : { 
			type : 		'input', 
			title : 	cmsmasters_events_schedule_shortcodes.event_schedule_field_notice_title, 
			descr : 	cmsmasters_events_schedule_shortcodes.event_schedule_field_notice_descr, 
			def : 		'', 
			required : 	false, 
			width : 	'half' 
		}, 
		// Additional Classes
		classes : { 
			type : 		'input', 
			title : 	cmsmasters_shortcodes.classes_title, 
			descr : 	cmsmasters_shortcodes.classes_descr, 
			def : 		'', 
			required : 	false, 
			width : 	'half' 
		} 
	} 
};

