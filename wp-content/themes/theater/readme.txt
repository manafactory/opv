/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version 	1.0.9
 * 
 * Theme Information
 * Created by CMSMasters
 * 
 */

To update your theme please use the files list from the FILE LOGS below and substitute/add the listed files on your server with the same files in the Updates folder.

Important: after you have updated the theme, in your admin panel please proceed to
Theme Settings - Fonts and click "Save" in any tab,
then proceed to 
Theme Settings - Colors and click "Save" in any tab here.


--------------------------------------

Version 1.0.9: files operations: 

	Theme Files edited:
		theater\framework\admin\inc\css\admin-theme-styles.css
		theater\framework\admin\options\css\cmsmasters-theme-options.css
		theater\framework\admin\settings\css\cmsmasters-theme-settings.css
		theater\framework\admin\settings\css\cmsmasters-theme-settings-rtl.css
		theater\gutenberg\cmsmasters-framework\theme-style\css\editor-style.css
		theater\gutenberg\cmsmasters-framework\theme-style\css\frontend-style.css
		theater\gutenberg\cmsmasters-framework\theme-style\css\module-rtl.css
		theater\style.css
		theater\theme-framework\theme-style\css\style.css
		theater\tribe-events\cmsmasters-framework\theme-style\css\plugin-style.css
		theater\framework\admin\options\js\cmsmasters-theme-options.js
		theater\framework\admin\settings\js\cmsmasters-theme-settings.js
		theater\js\jquery.script.js
		theater\theme-framework\theme-style\js\jquery.theme-script.js
		theater\gutenberg\cmsmasters-framework\theme-style\css\less\module-style.less
		theater\theme-framework\theme-style\css\less\style.less
		theater\tribe-events\cmsmasters-framework\theme-style\css\less\plugin-style.less
		theater\framework\admin\inc\config-functions.php
		theater\framework\admin\options\cmsmasters-theme-options.php
		theater\framework\admin\options\cmsmasters-theme-options-other.php
		theater\framework\admin\options\cmsmasters-theme-options-page.php
		theater\framework\admin\options\cmsmasters-theme-options-post.php
		theater\framework\admin\settings\cmsmasters-theme-settings.php
		theater\framework\admin\settings\cmsmasters-theme-settings-font.php
		theater\framework\admin\settings\cmsmasters-theme-settings-general.php
		theater\framework\admin\settings\inc\cmsmasters-helper-functions.php
		theater\framework\class\class-tgm-plugin-activation.php
		theater\framework\function\general-functions.php
		theater\framework\function\likes.php
		theater\framework\function\theme-categories-icon.php
		theater\framework\function\views.php
		theater\functions.php
		theater\gutenberg\cmsmasters-framework\theme-style\cmsmasters-module-functions.php
		theater\gutenberg\cmsmasters-framework\theme-style\function\module-colors.php
		theater\gutenberg\cmsmasters-framework\theme-style\function\module-fonts.php
		theater\search.php
		theater\sidebar.php
		theater\tc-events\cmsmasters-framework\theme-style\function\plugin-template-functions.php
		theater\theme-framework\theme-style\class\theme-widgets.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-counter.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-posts-slider.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-pricing-table-item.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-quote.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-quotes.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-stat.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-tab.php
		theater\theme-framework\theme-style\function\template-functions.php
		theater\theme-framework\theme-style\function\template-functions-post.php
		theater\theme-framework\theme-style\function\template-functions-profile.php
		theater\theme-framework\theme-style\function\template-functions-project.php
		theater\theme-framework\theme-style\function\template-functions-shortcodes.php
		theater\theme-framework\theme-style\postType\portfolio\project-grid.php
		theater\theme-framework\theme-style\theme-functions.php
		theater\theme-vars\plugin-activator.php
		theater\theme-vars\theme-style\admin\demo-content-importer.php
		theater\theme-vars\theme-style\admin\theme-settings-defaults.php
		theater\tribe-events\cmsmasters-framework\theme-style\cmsmasters-plugin-functions.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\modules\bar.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\pro\widgets\modules\single-event.php
		theater\woocommerce\cmsmasters-framework\theme-style\function\plugin-template-functions.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\content-single-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\content-widget-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\single-product-reviews.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\single-product\meta.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-image.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-thumbnails.php
		theater\woocommerce\content-widget-product.php
		theater\woocommerce\single-product-reviews.php
		theater\woocommerce\single-product\product-image.php
		theater\woocommerce\single-product\product-thumbnails.php
		theater\theme-vars\languages\theater.pot
		theater\readme.txt




	Plugins added:
		
		CMSMasters Custom Fonts
		CMSMasters Importer


	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.3.4
	
    Proceed to wp-content\plugins\cmsmasters-contact-form-builder
	and update all files in this folder to version 1.4.4
	
    Proceed to wp-content\plugins\cmsmasters-mega-menu
	and update all files in this folder to version 1.2.8

--------------------------------------

Version 1.0.8: files operations: 

	Theme Files edited:
		theater\style.css
		theater\theme-framework\theme-style\css\adaptive.css
		theater\theme-framework\theme-style\css\style.css
		theater\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		theater\js\smooth-sticky.min.js
		theater\theme-framework\theme-style\js\jquery.isotope.mode.js
		theater\theme-framework\theme-style\css\less\adaptive.less
		theater\theme-framework\theme-style\css\less\style.less
		theater\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		theater\framework\admin\options\cmsmasters-theme-options.php
		theater\framework\admin\settings\cmsmasters-theme-settings.php
		theater\framework\function\general-functions.php
		theater\sitemap.php
		theater\theme-vars\plugin-activator.php
		theater\tribe-events\cmsmasters-framework\theme-style\cmsmasters-plugin-functions.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\month\tooltip.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\pro\week\tooltip.php
		theater\theme-vars\languages\theater.pot
		theater\readme.txt
		
	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.2.8
	
    Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.8

--------------------------------------
Version 1.0.7: files operations: 

	Theme Files edited:
		theater\style.css
		theater\theme-framework\theme-style\css\adaptive.css
		theater\theme-framework\theme-style\css\style.css
		theater\theme-vars\theme-style\css\styles\theater.css
		theater\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		theater\js\jquery.script.js
		theater\js\smooth-sticky.min.js
		theater\woocommerce\cmsmasters-framework\theme-style\js\jquery.plugin-script.js
		theater\theme-framework\theme-style\css\less\adaptive.less
		theater\theme-framework\theme-style\css\less\style.less
		theater\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		theater\comments.php
		theater\framework\admin\inc\admin-scripts.php
		theater\framework\admin\settings\cmsmasters-theme-settings.php
		theater\framework\class\browser.php
		theater\framework\function\breadcrumbs.php
		theater\framework\function\general-functions.php
		theater\framework\function\likes.php
		theater\framework\function\views.php
		theater\functions.php
		theater\theme-framework\theme-style\class\theme-widgets.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-counter.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-posts-slider.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-pricing-table-item.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-quote.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-quotes.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-stat.php
		theater\theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-tab.php
		theater\theme-framework\theme-style\function\template-functions.php
		theater\theme-framework\theme-style\function\template-functions-post.php
		theater\theme-framework\theme-style\function\template-functions-profile.php
		theater\theme-framework\theme-style\function\template-functions-project.php
		theater\theme-framework\theme-style\function\template-functions-shortcodes.php
		theater\theme-framework\theme-style\function\template-functions-single.php
		theater\theme-framework\theme-style\function\theme-colors-primary.php
		theater\theme-framework\theme-style\postType\blog\post-single.php
		theater\theme-framework\theme-style\postType\portfolio\project-single.php
		theater\theme-framework\theme-style\postType\profile\profile-single.php
		theater\theme-framework\theme-style\template\footer.php
		theater\theme-framework\theme-style\template\header-bot.php
		theater\theme-framework\theme-style\template\header-mid.php
		theater\theme-vars\plugin-activator.php
		theater\theme-vars\theme-style\admin\theme-settings-defaults.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\modules\bar.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\month\single-event.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\pro\modules\meta\additional-fields.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\pro\week\single-event.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\pro\widgets\modules\single-event.php
		theater\woocommerce\archive-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\function\plugin-template-functions.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\archive-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\content-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\content-single-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\content-widget-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\single-product-reviews.php
		theater\woocommerce\content-product.php
		theater\woocommerce\content-single-product.php
		theater\readme.txt



	
	
	Proceed to wp-content\plugins\envato-market
	and update all files in this folder to version 2.0.0
	
	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.2.5
	
	Proceed to wp-content\plugins\cmsmasters-contact-form-builder
	and update all files in this folder to version 1.4.3
	
    Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.7.4

    Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 6.7.6

--------------------------------------

Version 1.0.6: files operations: 

	Theme Files edited:
		theater\framework\admin\options\css\cmsmasters-theme-options.css
		theater\framework\admin\settings\css\cmsmasters-theme-settings.css
		theater\style.css
		theater\theme-framework\theme-style\css\style.css
		theater\tribe-events\cmsmasters-framework\theme-style\css\plugin-style.css
		theater\woocommerce\cmsmasters-framework\theme-style\css\plugin-adaptive.css
		theater\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		theater\framework\admin\options\js\cmsmasters-theme-options.js
		theater\framework\admin\settings\js\cmsmasters-theme-settings.js
		theater\js\jquery.script.js
		theater\woocommerce\cmsmasters-framework\theme-style\js\jquery.plugin-script.js
		theater\theme-framework\theme-style\css\less\style.less
		theater\tribe-events\cmsmasters-framework\theme-style\css\less\plugin-style.less
		theater\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-adaptive.less
		theater\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		theater\framework\admin\options\cmsmasters-theme-options.php
		theater\framework\admin\settings\cmsmasters-theme-settings.php
		theater\framework\function\general-functions.php
		theater\framework\function\likes.php
		theater\framework\function\views.php
		theater\theme-framework\plugin-activator.php
		theater\woocommerce\archive-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\cmsmasters-plugin-functions.php
		theater\woocommerce\cmsmasters-framework\theme-style\function\plugin-colors.php
		theater\woocommerce\cmsmasters-framework\theme-style\function\plugin-fonts.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\archive-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\content-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\content-widget-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\loop\loop-start.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\loop\pagination.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\product-searchform.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-image.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-thumbnails.php
		theater\woocommerce\content-widget-product.php
		theater\woocommerce\global\wrapper-end.php
		theater\woocommerce\global\wrapper-start.php
		theater\woocommerce\loop\loop-start.php
		theater\woocommerce\loop\pagination.php
		theater\woocommerce\product-searchform.php
		theater\woocommerce\single-product\product-image.php
		theater\woocommerce\single-product\product-thumbnails.php
		theater\readme.txt


	
	
	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.1.9
	
	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.7.1

	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 6.7.0

--------------------------------------

Version 1.0.5: files operations: 

	Theme Files edited:
		theater\framework\admin\options\css\cmsmasters-theme-options.css
		theater\framework\admin\settings\css\cmsmasters-theme-settings.css
		theater\style.css
		theater\theme-framework\theme-style\css\adaptive.css
		theater\theme-framework\theme-style\css\style.css
		theater\tribe-events\cmsmasters-framework\theme-style\css\plugin-style.css
		theater\js\jquery.script.js
		theater\js\scrollspy.js
		theater\theme-framework\theme-style\js\jquery.isotope.mode.js
		theater\theme-framework\theme-style\css\less\adaptive.less
		theater\theme-framework\theme-style\css\less\style.less
		theater\tribe-events\cmsmasters-framework\theme-style\css\less\plugin-style.less
		theater\archive.php
		theater\archive-tc_events.php
		theater\author.php
		theater\cmsmasters-events-schedule\cmsmasters-framework\theme-style\postType\standard.php
		theater\cmsmasters-events-schedule\cmsmasters-framework\theme-style\templates\single-event-schedule.php
		theater\footer.php
		theater\framework\admin\options\cmsmasters-theme-options.php
		theater\framework\admin\settings\cmsmasters-theme-settings-single.php
		theater\framework\function\breadcrumbs.php
		theater\framework\function\general-functions.php
		theater\framework\function\pagination.php
		theater\header.php
		theater\image.php
		theater\index.php
		theater\page.php
		theater\search.php
		theater\sidebar.php
		theater\sidebar-bottom.php
		theater\single.php
		theater\single-event-schedule.php
		theater\single-profile.php
		theater\single-project.php
		theater\single-tc_events.php
		theater\sitemap.php
		theater\tc-events\cmsmasters-framework\theme-style\function\plugin-template-functions.php
		theater\tc-events\cmsmasters-framework\theme-style\templates\single-tc_events.php
		theater\theme-framework\plugin-activator.php
		theater\theme-framework\theme-style\admin\theme-settings-defaults.php
		theater\theme-framework\theme-style\class\theme-widgets.php
		theater\theme-framework\theme-style\function\single-comment.php
		theater\theme-framework\theme-style\function\template-functions.php
		theater\theme-framework\theme-style\function\template-functions-single.php
		theater\theme-framework\theme-style\function\theme-colors-primary.php
		theater\theme-framework\theme-style\postType\blog\post-default.php
		theater\theme-framework\theme-style\postType\blog\post-masonry.php
		theater\theme-framework\theme-style\postType\blog\post-puzzle.php
		theater\theme-framework\theme-style\postType\blog\post-single.php
		theater\theme-framework\theme-style\postType\blog\post-timeline.php
		theater\theme-framework\theme-style\postType\portfolio\project-grid.php
		theater\theme-framework\theme-style\postType\portfolio\project-puzzle.php
		theater\theme-framework\theme-style\postType\portfolio\project-single.php
		theater\theme-framework\theme-style\postType\posts-slider\slider-post.php
		theater\theme-framework\theme-style\postType\posts-slider\slider-project.php
		theater\theme-framework\theme-style\postType\profile\profile-horizontal.php
		theater\theme-framework\theme-style\postType\profile\profile-single.php
		theater\theme-framework\theme-style\postType\profile\profile-vertical.php
		theater\theme-framework\theme-style\postType\quote\quote-grid.php
		theater\theme-framework\theme-style\postType\quote\quote-slider-box.php
		theater\theme-framework\theme-style\postType\quote\quote-slider-center.php
		theater\theme-framework\theme-style\template\404.php
		theater\theme-framework\theme-style\template\header-bot.php
		theater\theme-framework\theme-style\template\header-mid.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\default-template.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\global\sidebar.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\global\wrapper-end.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\global\wrapper-start.php
		theater\theme-framework\languages\theater.pot
		theater\readme.txt



	
	Proceed to wp-content\plugins\cmsmasters-contact-form-builder
	and update all files in this folder to version 1.4.1
	
	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.1.8
	
	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.6.4

	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 6.6.5

--------------------------------------

Version 1.0.4: files operations: 

	Theme Files edited:
		theater\style.css
		theater\framework\admin\inc\js\wp-color-picker-alpha.js
		theater\theme-framework\plugin-activator.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\day\single-event.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\list\single-event.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\pro\map\single-event.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\pro\widgets\modules\single-event.php
		theater\tribe-events\cmsmasters-framework\theme-style\templates\widgets\list-widget.php
		theater\theme-framework\languages\theater.pot
		theater\readme.txt



	
	Proceed to wp-content\plugins\envato-market
	and update all files in this folder to version 1.0.0-RC2
	
	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.6.2

	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 6.6.1

--------------------------------------

Version 1.0.3: files operations: 

	Theme Files edited:
		theme-framework\plugin-activator.php
		theme-framework\plugins\cmsmasters-contact-form-builder.zip
		theme-framework\plugins\cmsmasters-content-composer.zip
		theme-framework\plugins\cmsmasters-events-schedule.zip
		theme-framework\plugins\cmsmasters-mega-menu.zip
		theme-framework\plugins\LayerSlider.zip
		theme-framework\plugins\revslider.zip
		woocommerce\cmsmasters-framework\theme-style\cmsmasters-c-c\cmsmasters-c-c-plugin-shortcodes.php
		woocommerce\cmsmasters-framework\theme-style\css\less\plugin-adaptive.less
		woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		woocommerce\cmsmasters-framework\theme-style\css\plugin-adaptive.css
		woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		woocommerce\cmsmasters-framework\theme-style\function\plugin-template-functions.php
		woocommerce\cmsmasters-framework\theme-style\templates\single-product-reviews.php
		woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-thumbnails.php
		woocommerce\single-product-reviews.php
		woocommerce\single-product\product-image.php

	Theme Files deleted:
		theme-framework\theme-style\css\styles\theatre.css

	Theme Files added:
		theme-framework\theme-style\css\styles\theater.css

	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.6.1

	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 6.6.0

--------------------------------------------------------------

Version 1.0.2: files operations: 


	Theme Files edited:
		theater\style.css
		theater\theme-framework\theme-style\css\style.css
		theater\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		theater\js\jquery.script.js
		theater\theme-framework\theme-style\js\jquery.isotope.mode.js
		theater\theme-framework\theme-style\js\jquery.theme-script.js
		theater\theme-framework\theme-style\css\less\style.less
		theater\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		theater\comments.php
		theater\framework\admin\settings\cmsmasters-theme-settings.php
		theater\framework\admin\settings\cmsmasters-theme-settings-single.php
		theater\framework\function\breadcrumbs.php
		theater\framework\function\general-functions.php
		theater\framework\function\likes.php
		theater\theme-framework\plugin-activator.php
		theater\theme-framework\theme-style\admin\theme-settings-defaults.php
		theater\theme-framework\theme-style\class\theme-widgets.php
		theater\theme-framework\theme-style\function\template-functions.php
		theater\theme-framework\theme-style\function\template-functions-post.php
		theater\theme-framework\theme-style\function\template-functions-profile.php
		theater\theme-framework\theme-style\function\template-functions-project.php
		theater\theme-framework\theme-style\function\template-functions-shortcodes.php
		theater\theme-framework\theme-style\function\theme-colors-primary.php
		theater\theme-framework\theme-style\postType\blog\post-default.php
		theater\theme-framework\theme-style\postType\portfolio\project-puzzle.php
		theater\theme-framework\theme-style\postType\portfolio\project-single.php
		theater\theme-framework\theme-style\postType\profile\profile-single.php
		theater\theme-framework\theme-style\template\archive.php
		theater\theme-framework\theme-style\theme-functions.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\content-single-product.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\single-product-reviews.php
		theater\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-image.php
		theater\theme-framework\languages\theater.pot
		theater\readme.txt
		
	Theme Files deleted:	
		theater\js\jqueryLibraries.min.js
		theater\js\jsLibraries.min.js
	
	Theme Files added:	
		theater\js\cmsmasters-hover-slider.min.js
		theater\js\debounced-resize.min.js
		theater\js\easing.min.js
		theater\js\easy-pie-chart.min.js
		theater\js\modernizr.min.js
		theater\js\mousewheel.min.js
		theater\js\owlcarousel.min.js
		theater\js\query-loader.min.js
		theater\js\request-animation-frame.min.js
		theater\js\respond.min.js
		theater\js\scroll-to.min.js
		theater\js\scrollspy.js
		theater\js\stellar.min.js
		theater\js\waypoints.min.js

		
	Proceed to wp-content\plugins\cmsmasters-contact-form-builder
	and update all files in this folder to version 1.4.0
	
	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.1.4
			
	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.5.2

	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 6.5.8	

--------------------------------------
Version 1.0.1: files operations: 


	Theme Files edited:
		theater\readme.txt
		theater\style.css
		theater\theme-framework\plugin-activator.php
		theater\theme-framework\theme-style\function\theme-colors-primary.php
		theater\theme-framework\theme-style\postType\blog\post-single.php
		
	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.1.3
			
	Proceed to wp-content\plugins\cmsmasters-contact-form-builder
	and update all files in this folder to version 1.3.9

	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 6.5.7	

--------------------------------------
Version 1.0: Release!

