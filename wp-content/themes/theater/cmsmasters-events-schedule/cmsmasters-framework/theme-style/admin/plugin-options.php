<?php 
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version 	1.0.0
 * 
 * Timetable Admin Options
 * Created by CMSMasters
 * 
 */


/* Filter for Options */
function theater_events_schedule_meta_fields($custom_all_meta_fields) {
	$custom_all_meta_fields_new = array();
	
	
	if (
		(isset($_GET['post_type']) && $_GET['post_type'] == 'event-schedule') || 
		(isset($_POST['post_type']) && $_POST['post_type'] == 'event-schedule') || 
		(isset($_GET['post']) && get_post_type($_GET['post']) == 'event-schedule') 
	) {
		foreach ($custom_all_meta_fields as $custom_all_meta_field) {
			if ($custom_all_meta_field['id'] == 'cmsmasters_event_schedule_speaker_heading') {
				$custom_all_meta_fields_new[] = array( 
					'label'	=> esc_html__('Speaker Image', 'theater'), 
					'desc'	=> '', 
					'id'	=> 'cmsmasters_event_schedule_image_link', 
					'type'	=> 'image', 
					'hide'	=> '', 
					'cancel' => 'true', 
					'std'	=> '', 
					'frame' => 'select', 
					'multiple' => false 
				);
				
				$custom_all_meta_fields_new[] = $custom_all_meta_field;
			} else {
				$custom_all_meta_fields_new[] = $custom_all_meta_field;
			}
		}
	} else {
		$custom_all_meta_fields_new = $custom_all_meta_fields;
	}
	
	
	return $custom_all_meta_fields_new;
}

add_filter('get_custom_all_meta_fields_filter', 'theater_events_schedule_meta_fields');

