<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version		1.0.5
 * 
 * CMSMasters Events Schedule Single Event Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_option = theater_get_global_options();


$event_schedule_dates = get_the_terms(get_the_ID(), 'es-date');

$cmsmasters_event_schedule_time = get_post_meta(get_the_ID(), 'cmsmasters_event_schedule_time', true);

$cmsmasters_event_schedule_speaker = get_post_meta(get_the_ID(), 'cmsmasters_event_schedule_speaker', true);

$cmsmasters_event_schedule_speaker_heading = get_post_meta(get_the_ID(), 'cmsmasters_event_schedule_speaker_heading', true);

$cmsmasters_event_schedule_image_link = get_post_meta(get_the_ID(), 'cmsmasters_event_schedule_image_link', true);

$cmsmasters_event_schedule_title = get_post_meta(get_the_ID(), 'cmsmasters_event_schedule_title', true);

$cmsmasters_event_schedule_sharing_box = get_post_meta(get_the_ID(), 'cmsmasters_event_schedule_sharing_box', true);

?>
<!-- Start Event Schedule Single Article -->
<article id="post-<?php the_ID();?>" <?php post_class('cmsmasters_open_event_schedule'); ?>>
	<?php
	if ($cmsmasters_event_schedule_time != '') {
		echo '<abbr class="cmsmasters_event_schedule_time">' . esc_html($cmsmasters_event_schedule_time) . '</abbr>' . "\n";
	}
	
	
	if ($cmsmasters_event_schedule_title == 'true') {
		echo '<h2 class="cmsmasters_event_schedule_title entry-title">' . get_the_title(get_the_ID()) . '</h2>' . "\n";
	}
	
	
	if (!post_password_required()) {
		if (has_post_thumbnail()) {
			theater_thumb(get_the_ID(), 'post-thumbnail', false, 'img_' . get_the_ID(), true, true, true, true, false);
		}
	}
	
	
	if (get_the_content() != '') {
		echo '<div class="cmsmasters_event_schedule_content entry-content">' . "\n";
			
			the_content();
			
			
			wp_link_pages(array( 
				'before' => '<div class="subpage_nav">' . '<strong>' . esc_html__('Pages', 'theater') . ':</strong>', 
				'after' => '</div>', 
				'link_before' => '<span>', 
				'link_after' => '</span>' 
			));
			
		echo '</div>';
	}
	
	
	if (
		(get_the_terms(get_the_ID(), 'es-date')) ||
		(get_the_terms(get_the_ID(), 'es-hall'))
	) {
		echo '<div class="cmsmasters_event_schedule_footer">';
	
			if ($cmsmasters_event_schedule_speaker != '') {
				echo '<div class="cmsmasters_event_schedule_speaker_wrap">' . "\n";
					
					if ($cmsmasters_event_schedule_image_link != '') {
						echo '<figure class="cmsmasters_speaker_image">' . 
							wp_get_attachment_image($cmsmasters_event_schedule_image_link, 'cmsmasters-small-thumb') . 
						'</figure>';
					}
					
					
					if ($cmsmasters_event_schedule_speaker != '') {
						echo '<span class="cmsmasters_event_schedule_speaker">' . esc_html($cmsmasters_event_schedule_speaker) . '</span>' . "\n";
					}
					
					
					if ($cmsmasters_event_schedule_speaker_heading != '') {
						echo '<span class="cmsmasters_event_schedule_speaker_heading">' . '<span> / </span>' . esc_html($cmsmasters_event_schedule_speaker_heading) . '</span>' . "\n";
					}
					
				echo '</div>';
			}
			
			echo '<div class="cmsmasters_event_schedule_cont_info">' . 
				'<span class="cmsmasters_event_schedule_date">' . 
					get_the_term_list(get_the_ID(), 'es-date', '', ', ', '') . 
				'</span>' .
				'<span class="cmsmasters_event_schedule_hall">' . 
					get_the_term_list(get_the_ID(), 'es-hall', '', ', ', '') . 
				'</span>' .
			'</div>' . 
		'</div>';
	}
	?>
</article>
<!-- Finish Event Schedule Single Article -->
<?php

if ($cmsmasters_event_schedule_sharing_box == 'true') {
	theater_sharing_box();
}


if ($cmsmasters_option['theater' . '_event_schedule_nav_box']) {
	theater_prev_next_posts();
}


comments_template();

