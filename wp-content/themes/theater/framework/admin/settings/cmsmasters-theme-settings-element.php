<?php 
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version 	1.0.0
 * 
 * Admin Panel Element Options
 * Created by CMSMasters
 * 
 */


function theater_options_element_tabs() {
	$tabs = array();
	
	$tabs['sidebar'] = esc_attr__('Sidebars', 'theater');
	
	if (class_exists('Cmsmasters_Content_Composer')) {
		$tabs['icon'] = esc_attr__('Social Icons', 'theater');
	}
	
	$tabs['lightbox'] = esc_attr__('Lightbox', 'theater');
	$tabs['sitemap'] = esc_attr__('Sitemap', 'theater');
	$tabs['error'] = esc_attr__('404', 'theater');
	$tabs['code'] = esc_attr__('Custom Codes', 'theater');
	
	if (class_exists('Cmsmasters_Form_Builder')) {
		$tabs['recaptcha'] = esc_attr__('reCAPTCHA', 'theater');
	}
	
	return apply_filters('cmsmasters_options_element_tabs_filter', $tabs);
}


function theater_options_element_sections() {
	$tab = theater_get_the_tab();
	
	switch ($tab) {
	case 'sidebar':
		$sections = array();
		
		$sections['sidebar_section'] = esc_attr__('Custom Sidebars', 'theater');
		
		break;
	case 'icon':
		$sections = array();
		
		$sections['icon_section'] = esc_attr__('Social Icons', 'theater');
		
		break;
	case 'lightbox':
		$sections = array();
		
		$sections['lightbox_section'] = esc_attr__('Theme Lightbox Options', 'theater');
		
		break;
	case 'sitemap':
		$sections = array();
		
		$sections['sitemap_section'] = esc_attr__('Sitemap Page Options', 'theater');
		
		break;
	case 'error':
		$sections = array();
		
		$sections['error_section'] = esc_attr__('404 Error Page Options', 'theater');
		
		break;
	case 'code':
		$sections = array();
		
		$sections['code_section'] = esc_attr__('Custom Codes', 'theater');
		
		break;
	case 'recaptcha':
		$sections = array();
		
		$sections['recaptcha_section'] = esc_attr__('Form Builder Plugin reCAPTCHA Keys', 'theater');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return apply_filters('cmsmasters_options_element_sections_filter', $sections, $tab);	
} 


function theater_options_element_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = theater_get_the_tab();
	}
	
	
	$options = array();
	
	
	$defaults = theater_settings_element_defaults();
	
	
	switch ($tab) {
	case 'sidebar':
		$options[] = array( 
			'section' => 'sidebar_section', 
			'id' => 'theater' . '_sidebar', 
			'title' => esc_html__('Custom Sidebars', 'theater'), 
			'desc' => '', 
			'type' => 'sidebar', 
			'std' => $defaults[$tab]['theater' . '_sidebar'] 
		);
		
		break;
	case 'icon':
		$options[] = array( 
			'section' => 'icon_section', 
			'id' => 'theater' . '_social_icons', 
			'title' => esc_html__('Social Icons', 'theater'), 
			'desc' => '', 
			'type' => 'social', 
			'std' => $defaults[$tab]['theater' . '_social_icons'] 
		);
		
		break;
	case 'lightbox':
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_skin', 
			'title' => esc_html__('Skin', 'theater'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_skin'], 
			'choices' => array( 
				esc_html__('Dark', 'theater') . '|dark', 
				esc_html__('Light', 'theater') . '|light', 
				esc_html__('Mac', 'theater') . '|mac', 
				esc_html__('Metro Black', 'theater') . '|metro-black', 
				esc_html__('Metro White', 'theater') . '|metro-white', 
				esc_html__('Parade', 'theater') . '|parade', 
				esc_html__('Smooth', 'theater') . '|smooth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_path', 
			'title' => esc_html__('Path', 'theater'), 
			'desc' => esc_html__('Sets path for switching windows', 'theater'), 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_path'], 
			'choices' => array( 
				esc_html__('Vertical', 'theater') . '|vertical', 
				esc_html__('Horizontal', 'theater') . '|horizontal' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_infinite', 
			'title' => esc_html__('Infinite', 'theater'), 
			'desc' => esc_html__('Sets the ability to infinite the group', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_infinite'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_aspect_ratio', 
			'title' => esc_html__('Keep Aspect Ratio', 'theater'), 
			'desc' => esc_html__('Sets the resizing method used to keep aspect ratio within the viewport', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_aspect_ratio'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_mobile_optimizer', 
			'title' => esc_html__('Mobile Optimizer', 'theater'), 
			'desc' => esc_html__('Make lightboxes optimized for giving better experience with mobile devices', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_mobile_optimizer'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_max_scale', 
			'title' => esc_html__('Max Scale', 'theater'), 
			'desc' => esc_html__('Sets the maximum viewport scale of the content', 'theater'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_max_scale'], 
			'min' => 0.1, 
			'max' => 2, 
			'step' => 0.05 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_min_scale', 
			'title' => esc_html__('Min Scale', 'theater'), 
			'desc' => esc_html__('Sets the minimum viewport scale of the content', 'theater'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_min_scale'], 
			'min' => 0.1, 
			'max' => 2, 
			'step' => 0.05 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_inner_toolbar', 
			'title' => esc_html__('Inner Toolbar', 'theater'), 
			'desc' => esc_html__('Bring buttons into windows, or let them be over the overlay', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_inner_toolbar'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_smart_recognition', 
			'title' => esc_html__('Smart Recognition', 'theater'), 
			'desc' => esc_html__('Sets content auto recognize from web pages', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_smart_recognition'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_fullscreen_one_slide', 
			'title' => esc_html__('Fullscreen One Slide', 'theater'), 
			'desc' => esc_html__('Decide to fullscreen only one slide or hole gallery the fullscreen mode', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_fullscreen_one_slide'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_fullscreen_viewport', 
			'title' => esc_html__('Fullscreen Viewport', 'theater'), 
			'desc' => esc_html__('Sets the resizing method used to fit content within the fullscreen mode', 'theater'), 
			'type' => 'select', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_fullscreen_viewport'], 
			'choices' => array( 
				esc_html__('Center', 'theater') . '|center', 
				esc_html__('Fit', 'theater') . '|fit', 
				esc_html__('Fill', 'theater') . '|fill', 
				esc_html__('Stretch', 'theater') . '|stretch' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_controls_toolbar', 
			'title' => esc_html__('Toolbar Controls', 'theater'), 
			'desc' => esc_html__('Sets buttons be available or not', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_controls_toolbar'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_controls_arrows', 
			'title' => esc_html__('Arrow Controls', 'theater'), 
			'desc' => esc_html__('Enable the arrow buttons', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_controls_arrows'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_controls_fullscreen', 
			'title' => esc_html__('Fullscreen Controls', 'theater'), 
			'desc' => esc_html__('Sets the fullscreen button', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_controls_fullscreen'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_controls_thumbnail', 
			'title' => esc_html__('Thumbnails Controls', 'theater'), 
			'desc' => esc_html__('Sets the thumbnail navigation', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_controls_thumbnail'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_controls_keyboard', 
			'title' => esc_html__('Keyboard Controls', 'theater'), 
			'desc' => esc_html__('Sets the keyboard navigation', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_controls_keyboard'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_controls_mousewheel', 
			'title' => esc_html__('Mouse Wheel Controls', 'theater'), 
			'desc' => esc_html__('Sets the mousewheel navigation', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_controls_mousewheel'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_controls_swipe', 
			'title' => esc_html__('Swipe Controls', 'theater'), 
			'desc' => esc_html__('Sets the swipe navigation', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_controls_swipe'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'theater' . '_ilightbox_controls_slideshow', 
			'title' => esc_html__('Slideshow Controls', 'theater'), 
			'desc' => esc_html__('Enable the slideshow feature and button', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_ilightbox_controls_slideshow'] 
		);
		
		break;
	case 'sitemap':
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'theater' . '_sitemap_nav', 
			'title' => esc_html__('Website Pages', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_sitemap_nav'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'theater' . '_sitemap_categs', 
			'title' => esc_html__('Blog Archives by Categories', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_sitemap_categs'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'theater' . '_sitemap_tags', 
			'title' => esc_html__('Blog Archives by Tags', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_sitemap_tags'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'theater' . '_sitemap_month', 
			'title' => esc_html__('Blog Archives by Month', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_sitemap_month'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'theater' . '_sitemap_pj_categs', 
			'title' => esc_html__('Portfolio Archives by Categories', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_sitemap_pj_categs'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'theater' . '_sitemap_pj_tags', 
			'title' => esc_html__('Portfolio Archives by Tags', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_sitemap_pj_tags'] 
		);
		
		break;
	case 'error':
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_color', 
			'title' => esc_html__('Text Color', 'theater'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['theater' . '_error_color'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_bg_color', 
			'title' => esc_html__('Background Color', 'theater'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['theater' . '_error_bg_color'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_bg_img_enable', 
			'title' => esc_html__('Background Image Visibility', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_error_bg_img_enable'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_bg_image', 
			'title' => esc_html__('Background Image', 'theater'), 
			'desc' => esc_html__('Choose your custom error page background image.', 'theater'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['theater' . '_error_bg_image'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_bg_rep', 
			'title' => esc_html__('Background Repeat', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_error_bg_rep'], 
			'choices' => array( 
				esc_html__('No Repeat', 'theater') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'theater') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'theater') . '|repeat-y', 
				esc_html__('Repeat', 'theater') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_bg_pos', 
			'title' => esc_html__('Background Position', 'theater'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['theater' . '_error_bg_pos'], 
			'choices' => array( 
				esc_html__('Top Left', 'theater') . '|top left', 
				esc_html__('Top Center', 'theater') . '|top center', 
				esc_html__('Top Right', 'theater') . '|top right', 
				esc_html__('Center Left', 'theater') . '|center left', 
				esc_html__('Center Center', 'theater') . '|center center', 
				esc_html__('Center Right', 'theater') . '|center right', 
				esc_html__('Bottom Left', 'theater') . '|bottom left', 
				esc_html__('Bottom Center', 'theater') . '|bottom center', 
				esc_html__('Bottom Right', 'theater') . '|bottom right' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_bg_att', 
			'title' => esc_html__('Background Attachment', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_error_bg_att'], 
			'choices' => array( 
				esc_html__('Scroll', 'theater') . '|scroll', 
				esc_html__('Fixed', 'theater') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_bg_size', 
			'title' => esc_html__('Background Size', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_error_bg_size'], 
			'choices' => array( 
				esc_html__('Auto', 'theater') . '|auto', 
				esc_html__('Cover', 'theater') . '|cover', 
				esc_html__('Contain', 'theater') . '|contain' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_search', 
			'title' => esc_html__('Search Line', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_error_search'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_sitemap_button', 
			'title' => esc_html__('Sitemap Button', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_error_sitemap_button'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'theater' . '_error_sitemap_link', 
			'title' => esc_html__('Sitemap Page URL', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_error_sitemap_link'], 
			'class' => '' 
		);
		
		break;
	case 'code':
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'theater' . '_custom_css', 
			'title' => esc_html__('Custom CSS', 'theater'), 
			'desc' => '', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['theater' . '_custom_css'], 
			'class' => 'allowlinebreaks' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'theater' . '_custom_js', 
			'title' => esc_html__('Custom JavaScript', 'theater'), 
			'desc' => '', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['theater' . '_custom_js'], 
			'class' => 'allowlinebreaks' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'theater' . '_gmap_api_key', 
			'title' => esc_html__('Google Maps API key', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_gmap_api_key'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'theater' . '_api_key', 
			'title' => esc_html__('Twitter API key', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_api_key'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'theater' . '_api_secret', 
			'title' => esc_html__('Twitter API secret', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_api_secret'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'theater' . '_access_token', 
			'title' => esc_html__('Twitter Access token', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_access_token'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'theater' . '_access_token_secret', 
			'title' => esc_html__('Twitter Access token secret', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_access_token_secret'], 
			'class' => '' 
		);
		
		break;
	case 'recaptcha':
		$options[] = array( 
			'section' => 'recaptcha_section', 
			'id' => 'theater' . '_recaptcha_public_key', 
			'title' => esc_html__('reCAPTCHA Public Key', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_recaptcha_public_key'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'recaptcha_section', 
			'id' => 'theater' . '_recaptcha_private_key', 
			'title' => esc_html__('reCAPTCHA Private Key', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_recaptcha_private_key'], 
			'class' => '' 
		);
		
		break;
	}
	
	return apply_filters('cmsmasters_options_element_fields_filter', $options, $tab);	
}

