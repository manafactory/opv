<?php 
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version		1.0.9
 * 
 * Admin Panel Fonts Options
 * Created by CMSMasters
 * 
 */


function theater_options_font_tabs() {
	$tabs = array();
	
	$tabs['content'] = esc_attr__('Content', 'theater');
	$tabs['link'] = esc_attr__('Links', 'theater');
	$tabs['nav'] = esc_attr__('Navigation', 'theater');
	$tabs['heading'] = esc_attr__('Heading', 'theater');
	$tabs['other'] = esc_attr__('Other', 'theater');
	$tabs['google'] = esc_attr__('Google Fonts', 'theater');
	
	return apply_filters('cmsmasters_options_font_tabs_filter', $tabs);
}


function theater_options_font_sections() {
	$tab = theater_get_the_tab();
	
	switch ($tab) {
	case 'content':
		$sections = array();
		
		$sections['content_section'] = esc_html__('Content Font Options', 'theater');
		
		break;
	case 'link':
		$sections = array();
		
		$sections['link_section'] = esc_html__('Links Font Options', 'theater');
		
		break;
	case 'nav':
		$sections = array();
		
		$sections['nav_section'] = esc_html__('Navigation Font Options', 'theater');
		
		break;
	case 'heading':
		$sections = array();
		
		$sections['heading_section'] = esc_html__('Headings Font Options', 'theater');
		
		break;
	case 'other':
		$sections = array();
		
		$sections['other_section'] = esc_html__('Other Fonts Options', 'theater');
		
		break;
	case 'google':
		$sections = array();
		
		$sections['google_section'] = esc_html__('Serving Google Fonts from CDN', 'theater');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return apply_filters('cmsmasters_options_font_sections_filter', $sections, $tab);
} 


function theater_options_font_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = theater_get_the_tab();
	}
	
	
	$options = array();
	
	
	$defaults = theater_settings_font_defaults();
	
	
	switch ($tab) {
	case 'content':
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_content_font', 
			'title' => esc_html__('Main Content Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_content_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style' 
			) 
		);
		
		break;
	case 'link':
		$options[] = array( 
			'section' => 'link_section', 
			'id' => 'theater' . '_link_font', 
			'title' => esc_html__('Links Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_link_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'link_section', 
			'id' => 'theater' . '_link_hover_decoration', 
			'title' => esc_html__('Links Hover Text Decoration', 'theater'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['theater' . '_link_hover_decoration'], 
			'choices' => theater_text_decoration_list() 
		);
		
		break;
	case 'nav':
		$options[] = array( 
			'section' => 'nav_section', 
			'id' => 'theater' . '_nav_title_font', 
			'title' => esc_html__('Navigation Title Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_nav_title_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform' 
			) 
		);
		
		$options[] = array( 
			'section' => 'nav_section', 
			'id' => 'theater' . '_nav_dropdown_font', 
			'title' => esc_html__('Navigation Dropdown Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_nav_dropdown_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform' 
			) 
		);
		
		break;
	case 'heading':
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'theater' . '_h1_font', 
			'title' => esc_html__('H1 Tag Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_h1_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'theater' . '_h2_font', 
			'title' => esc_html__('H2 Tag Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_h2_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'theater' . '_h3_font', 
			'title' => esc_html__('H3 Tag Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_h3_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'theater' . '_h4_font', 
			'title' => esc_html__('H4 Tag Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_h4_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'theater' . '_h5_font', 
			'title' => esc_html__('H5 Tag Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_h5_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'theater' . '_h6_font', 
			'title' => esc_html__('H6 Tag Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_h6_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		break;
	case 'other':
		$options[] = array( 
			'section' => 'other_section', 
			'id' => 'theater' . '_button_font', 
			'title' => esc_html__('Button Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_button_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform' 
			) 
		);
		
		$options[] = array( 
			'section' => 'other_section', 
			'id' => 'theater' . '_small_font', 
			'title' => esc_html__('Small Tag Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_small_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform' 
			) 
		);
		
		$options[] = array( 
			'section' => 'other_section', 
			'id' => 'theater' . '_input_font', 
			'title' => esc_html__('Text Fields Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_input_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style' 
			) 
		);
		
		$options[] = array( 
			'section' => 'other_section', 
			'id' => 'theater' . '_quote_font', 
			'title' => esc_html__('Blockquote Font', 'theater'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['theater' . '_quote_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style' 
			) 
		);
		
		break;
	case 'google':
		$options[] = array( 
			'section' => 'google_section', 
			'id' => 'theater' . '_google_web_fonts', 
			'title' => esc_html__('Google Fonts', 'theater'), 
			'desc' => '', 
			'type' => 'google_web_fonts', 
			'std' => $defaults[$tab]['theater' . '_google_web_fonts'] 
		);
		
		$options[] = array( 
			'section' => 'google_section', 
			'id' => 'theater' . '_google_web_fonts_subset', 
			'title' => esc_html__('Google Fonts Subset', 'theater'), 
			'desc' => '', 
			'type' => 'select_multiple', 
			'std' => '', 
			'choices' => array( 
				esc_html__('Latin Extended', 'theater') . '|' . 'latin-ext', 
				esc_html__('Arabic', 'theater') . '|' . 'arabic', 
				esc_html__('Cyrillic', 'theater') . '|' . 'cyrillic', 
				esc_html__('Cyrillic Extended', 'theater') . '|' . 'cyrillic-ext', 
				esc_html__('Greek', 'theater') . '|' . 'greek', 
				esc_html__('Greek Extended', 'theater') . '|' . 'greek-ext', 
				esc_html__('Vietnamese', 'theater') . '|' . 'vietnamese', 
				esc_html__('Japanese', 'theater') . '|' . 'japanese', 
				esc_html__('Korean', 'theater') . '|' . 'korean', 
				esc_html__('Thai', 'theater') . '|' . 'thai', 
				esc_html__('Bengali', 'theater') . '|' . 'bengali', 
				esc_html__('Devanagari', 'theater') . '|' . 'devanagari', 
				esc_html__('Gujarati', 'theater') . '|' . 'gujarati', 
				esc_html__('Gurmukhi', 'theater') . '|' . 'gurmukhi', 
				esc_html__('Hebrew', 'theater') . '|' . 'hebrew', 
				esc_html__('Kannada', 'theater') . '|' . 'kannada', 
				esc_html__('Khmer', 'theater') . '|' . 'khmer', 
				esc_html__('Malayalam', 'theater') . '|' . 'malayalam', 
				esc_html__('Myanmar', 'theater') . '|' . 'myanmar', 
				esc_html__('Oriya', 'theater') . '|' . 'oriya', 
				esc_html__('Sinhala', 'theater') . '|' . 'sinhala', 
				esc_html__('Tamil', 'theater') . '|' . 'tamil', 
				esc_html__('Telugu', 'theater') . '|' . 'telugu' 
			) 
		);
		
		break;
	}
	
	return apply_filters('cmsmasters_options_font_fields_filter', $options, $tab);	
}

