<?php 
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version 	1.0.9
 * 
 * Admin Panel General Options
 * Created by CMSMasters
 * 
 */


function theater_options_general_tabs() {
	$cmsmasters_option = theater_get_global_options();
	
	$tabs = array();
	
	$tabs['general'] = esc_attr__('General', 'theater');
	
	if ($cmsmasters_option['theater' . '_theme_layout'] === 'boxed') {
		$tabs['bg'] = esc_attr__('Background', 'theater');
	}
	
	if (CMSMASTERS_THEME_STYLE_COMPATIBILITY) {
		$tabs['theme_style'] = esc_attr__('Theme Style', 'theater');
	}
	
	$tabs['header'] = esc_attr__('Header', 'theater');
	$tabs['content'] = esc_attr__('Content', 'theater');
	$tabs['footer'] = esc_attr__('Footer', 'theater');
	
	return apply_filters('cmsmasters_options_general_tabs_filter', $tabs);
}


function theater_options_general_sections() {
	$tab = theater_get_the_tab();
	
	switch ($tab) {
	case 'general':
		$sections = array();
		
		$sections['general_section'] = esc_attr__('General Options', 'theater');
		
		break;
	case 'bg':
		$sections = array();
		
		$sections['bg_section'] = esc_attr__('Background Options', 'theater');
		
		break;
	case 'theme_style':
		$sections = array();
		
		$sections['theme_style_section'] = esc_attr__('Theme Design Style', 'theater');
		
		break;
	case 'header':
		$sections = array();
		
		$sections['header_section'] = esc_attr__('Header Options', 'theater');
		
		break;
	case 'content':
		$sections = array();
		
		$sections['content_section'] = esc_attr__('Content Options', 'theater');
		
		break;
	case 'footer':
		$sections = array();
		
		$sections['footer_section'] = esc_attr__('Footer Options', 'theater');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return apply_filters('cmsmasters_options_general_sections_filter', $sections, $tab);
} 


function theater_options_general_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = theater_get_the_tab();
	}
	
	$options = array();
	
	
	$defaults = theater_settings_general_defaults();
	
	
	switch ($tab) {
	case 'general':
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'theater' . '_theme_layout', 
			'title' => esc_html__('Theme Layout', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_theme_layout'], 
			'choices' => array( 
				esc_html__('Liquid', 'theater') . '|liquid', 
				esc_html__('Boxed', 'theater') . '|boxed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'theater' . '_logo_type', 
			'title' => esc_html__('Logo Type', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_logo_type'], 
			'choices' => array( 
				esc_html__('Image', 'theater') . '|image', 
				esc_html__('Text', 'theater') . '|text' 
			) 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'theater' . '_logo_url', 
			'title' => esc_html__('Logo Image', 'theater'), 
			'desc' => esc_html__('Choose your website logo image.', 'theater'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['theater' . '_logo_url'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'theater' . '_logo_url_retina', 
			'title' => esc_html__('Retina Logo Image', 'theater'), 
			'desc' => esc_html__('Choose logo image for retina displays. Logo for Retina displays should be twice the size of the default one.', 'theater'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['theater' . '_logo_url_retina'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'theater' . '_logo_title', 
			'title' => esc_html__('Logo Title', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_logo_title'], 
			'class' => 'nohtml' 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'theater' . '_logo_subtitle', 
			'title' => esc_html__('Logo Subtitle', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_logo_subtitle'], 
			'class' => 'nohtml' 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'theater' . '_logo_custom_color', 
			'title' => esc_html__('Custom Text Colors', 'theater'), 
			'desc' => esc_html__('enable', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_logo_custom_color'] 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'theater' . '_logo_title_color', 
			'title' => esc_html__('Logo Title Color', 'theater'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['theater' . '_logo_title_color'] 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'theater' . '_logo_subtitle_color', 
			'title' => esc_html__('Logo Subtitle Color', 'theater'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['theater' . '_logo_subtitle_color'] 
		);
		
		break;
	case 'bg':
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'theater' . '_bg_col', 
			'title' => esc_html__('Background Color', 'theater'), 
			'desc' => '', 
			'type' => 'color', 
			'std' => $defaults[$tab]['theater' . '_bg_col'] 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'theater' . '_bg_img_enable', 
			'title' => esc_html__('Background Image Visibility', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_bg_img_enable'] 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'theater' . '_bg_img', 
			'title' => esc_html__('Background Image', 'theater'), 
			'desc' => esc_html__('Choose your custom website background image url.', 'theater'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['theater' . '_bg_img'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'theater' . '_bg_rep', 
			'title' => esc_html__('Background Repeat', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_bg_rep'], 
			'choices' => array( 
				esc_html__('No Repeat', 'theater') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'theater') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'theater') . '|repeat-y', 
				esc_html__('Repeat', 'theater') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'theater' . '_bg_pos', 
			'title' => esc_html__('Background Position', 'theater'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['theater' . '_bg_pos'], 
			'choices' => array( 
				esc_html__('Top Left', 'theater') . '|top left', 
				esc_html__('Top Center', 'theater') . '|top center', 
				esc_html__('Top Right', 'theater') . '|top right', 
				esc_html__('Center Left', 'theater') . '|center left', 
				esc_html__('Center Center', 'theater') . '|center center', 
				esc_html__('Center Right', 'theater') . '|center right', 
				esc_html__('Bottom Left', 'theater') . '|bottom left', 
				esc_html__('Bottom Center', 'theater') . '|bottom center', 
				esc_html__('Bottom Right', 'theater') . '|bottom right' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'theater' . '_bg_att', 
			'title' => esc_html__('Background Attachment', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_bg_att'], 
			'choices' => array( 
				esc_html__('Scroll', 'theater') . '|scroll', 
				esc_html__('Fixed', 'theater') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'theater' . '_bg_size', 
			'title' => esc_html__('Background Size', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_bg_size'], 
			'choices' => array( 
				esc_html__('Auto', 'theater') . '|auto', 
				esc_html__('Cover', 'theater') . '|cover', 
				esc_html__('Contain', 'theater') . '|contain' 
			) 
		);
		
		break;
	case 'theme_style':
		$options[] = array( 
			'section' => 'theme_style_section', 
			'id' => 'theater' . '_theme_style', 
			'title' => esc_html__('Choose Theme Style', 'theater'), 
			'desc' => '', 
			'type' => 'select_theme_style', 
			'std' => '', 
			'choices' => theater_all_theme_styles() 
		);
		
		break;
	case 'header':
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_fixed_header', 
			'title' => esc_html__('Fixed Header', 'theater'), 
			'desc' => esc_html__('enable', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_fixed_header'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_overlaps', 
			'title' => esc_html__('Header Overlaps Content by Default', 'theater'), 
			'desc' => esc_html__('enable', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_header_overlaps'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_top_line', 
			'title' => esc_html__('Top Line', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_header_top_line'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_top_height', 
			'title' => esc_html__('Top Height', 'theater'), 
			'desc' => esc_html__('pixels', 'theater'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['theater' . '_header_top_height'], 
			'min' => '10' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_top_line_short_info', 
			'title' => esc_html__('Top Short Info', 'theater'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'theater') . '</strong>', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['theater' . '_header_top_line_short_info'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_top_line_add_cont', 
			'title' => esc_html__('Top Additional Content', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_header_top_line_add_cont'], 
			'choices' => array( 
				esc_html__('None', 'theater') . '|none', 
				esc_html__('Top Line Social Icons (will be shown if Cmsmasters Content Composer plugin is active)', 'theater') . '|social', 
				esc_html__('Top Line Navigation (will be shown if set in Appearance - Menus tab)', 'theater') . '|nav' 
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_styles', 
			'title' => esc_html__('Header Styles', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_header_styles'], 
			'choices' => array( 
				esc_html__('Default Style', 'theater') . '|default', 
				esc_html__('Compact Style Left Navigation', 'theater') . '|l_nav', 
				esc_html__('Compact Style Right Navigation', 'theater') . '|r_nav', 
				esc_html__('Compact Style Center Navigation', 'theater') . '|c_nav'
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_mid_height', 
			'title' => esc_html__('Header Middle Height', 'theater'), 
			'desc' => esc_html__('pixels', 'theater'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['theater' . '_header_mid_height'], 
			'min' => '40' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_bot_height', 
			'title' => esc_html__('Header Bottom Height', 'theater'), 
			'desc' => esc_html__('pixels', 'theater'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['theater' . '_header_bot_height'], 
			'min' => '20' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_search', 
			'title' => esc_html__('Header Search', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_header_search'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_add_cont', 
			'title' => esc_html__('Header Additional Content', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_header_add_cont'], 
			'choices' => array( 
				esc_html__('None', 'theater') . '|none', 
				esc_html__('Header Social Icons (will be shown if Cmsmasters Content Composer plugin is active)', 'theater') . '|social', 
				esc_html__('Header Custom HTML', 'theater') . '|cust_html' 
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'theater' . '_header_add_cont_cust_html', 
			'title' => esc_html__('Header Custom HTML', 'theater'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'theater') . '</strong>', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['theater' . '_header_add_cont_cust_html'], 
			'class' => '' 
		);
		
		break;
	case 'content':
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_layout', 
			'title' => esc_html__('Layout Type by Default', 'theater'), 
			'desc' => esc_html__('Choosing layout with a sidebar please make sure to add widgets to the Sidebar in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'theater'), 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['theater' . '_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_archives_layout', 
			'title' => esc_html__('Archives Layout Type', 'theater'), 
			'desc' => esc_html__('Choosing layout with a sidebar please make sure to add widgets to the Archive Sidebar in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'theater'), 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['theater' . '_archives_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_search_layout', 
			'title' => esc_html__('Search Layout Type', 'theater'), 
			'desc' => esc_html__('Choosing layout with a sidebar please make sure to add widgets to the Search Sidebar in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'theater'), 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['theater' . '_search_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_other_layout', 
			'title' => esc_html__('Other Layout Type', 'theater'), 
			'desc' => esc_html__('Layout for pages of non-listed types. Choosing layout with a sidebar please make sure to add widgets to the Sidebar in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'theater'), 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['theater' . '_other_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'theater') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_heading_alignment', 
			'title' => esc_html__('Heading Alignment by Default', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_heading_alignment'], 
			'choices' => array( 
				esc_html__('Left', 'theater') . '|left', 
				esc_html__('Right', 'theater') . '|right', 
				esc_html__('Center', 'theater') . '|center' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_heading_scheme', 
			'title' => esc_html__('Heading Custom Color Scheme by Default', 'theater'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['theater' . '_heading_scheme'], 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_heading_bg_image_enable', 
			'title' => esc_html__('Heading Background Image Visibility by Default', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_heading_bg_image_enable'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_heading_bg_image', 
			'title' => esc_html__('Heading Background Image by Default', 'theater'), 
			'desc' => esc_html__('Choose your custom heading background image by default.', 'theater'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['theater' . '_heading_bg_image'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_heading_bg_repeat', 
			'title' => esc_html__('Heading Background Repeat by Default', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_heading_bg_repeat'], 
			'choices' => array( 
				esc_html__('No Repeat', 'theater') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'theater') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'theater') . '|repeat-y', 
				esc_html__('Repeat', 'theater') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_heading_bg_attachment', 
			'title' => esc_html__('Heading Background Attachment by Default', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_heading_bg_attachment'], 
			'choices' => array( 
				esc_html__('Scroll', 'theater') . '|scroll', 
				esc_html__('Fixed', 'theater') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_heading_bg_size', 
			'title' => esc_html__('Heading Background Size by Default', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_heading_bg_size'], 
			'choices' => array( 
				esc_html__('Auto', 'theater') . '|auto', 
				esc_html__('Cover', 'theater') . '|cover', 
				esc_html__('Contain', 'theater') . '|contain' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_heading_bg_color', 
			'title' => esc_html__('Heading Background Color Overlay by Default', 'theater'), 
			'desc' => '',  
			'type' => 'rgba', 
			'std' => $defaults[$tab]['theater' . '_heading_bg_color'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_heading_height', 
			'title' => esc_html__('Heading Height by Default', 'theater'), 
			'desc' => esc_html__('pixels', 'theater'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['theater' . '_heading_height'], 
			'min' => '0' 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_breadcrumbs', 
			'title' => esc_html__('Breadcrumbs Visibility by Default', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_breadcrumbs'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_bottom_scheme', 
			'title' => esc_html__('Bottom Custom Color Scheme', 'theater'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['theater' . '_bottom_scheme'], 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_bottom_sidebar', 
			'title' => esc_html__('Bottom Sidebar Visibility by Default', 'theater'), 
			'desc' => esc_html__('show', 'theater') . '<br><br>' . esc_html__('Please make sure to add widgets in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_bottom_sidebar'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'theater' . '_bottom_sidebar_layout', 
			'title' => esc_html__('Bottom Sidebar Layout by Default', 'theater'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['theater' . '_bottom_sidebar_layout'], 
			'choices' => array( 
				'1/1|11', 
				'1/2 + 1/2|1212', 
				'1/3 + 2/3|1323', 
				'2/3 + 1/3|2313', 
				'1/4 + 3/4|1434', 
				'3/4 + 1/4|3414', 
				'1/3 + 1/3 + 1/3|131313', 
				'1/2 + 1/4 + 1/4|121414', 
				'1/4 + 1/2 + 1/4|141214', 
				'1/4 + 1/4 + 1/2|141412', 
				'1/4 + 1/4 + 1/4 + 1/4|14141414' 
			) 
		);
		
		break;
	case 'footer':
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_scheme', 
			'title' => esc_html__('Footer Custom Color Scheme', 'theater'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['theater' . '_footer_scheme'], 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_type', 
			'title' => esc_html__('Footer Type', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_footer_type'], 
			'choices' => array( 
				esc_html__('Default', 'theater') . '|default', 
				esc_html__('Small', 'theater') . '|small' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_additional_content', 
			'title' => esc_html__('Footer Additional Content', 'theater'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['theater' . '_footer_additional_content'], 
			'choices' => array( 
				esc_html__('None', 'theater') . '|none', 
				esc_html__('Footer Navigation (will be shown if set in Appearance - Menus tab)', 'theater') . '|nav', 
				esc_html__('Social Icons (will be shown if Cmsmasters Content Composer plugin is active)', 'theater') . '|social', 
				esc_html__('Custom HTML', 'theater') . '|text' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_logo', 
			'title' => esc_html__('Footer Logo', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_footer_logo'] 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_logo_url', 
			'title' => esc_html__('Footer Logo', 'theater'), 
			'desc' => esc_html__('Choose your website footer logo image.', 'theater'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['theater' . '_footer_logo_url'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_logo_url_retina', 
			'title' => esc_html__('Footer Logo for Retina', 'theater'), 
			'desc' => esc_html__('Choose your website footer logo image for retina.', 'theater'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['theater' . '_footer_logo_url_retina'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_nav', 
			'title' => esc_html__('Footer Navigation', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_footer_nav'] 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_social', 
			'title' => esc_html__('Footer Social Icons (will be shown if Cmsmasters Content Composer plugin is active)', 'theater'), 
			'desc' => esc_html__('show', 'theater'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['theater' . '_footer_social'] 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_html', 
			'title' => esc_html__('Footer Custom HTML', 'theater'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'theater') . '</strong>', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['theater' . '_footer_html'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'theater' . '_footer_copyright', 
			'title' => esc_html__('Copyright Text', 'theater'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['theater' . '_footer_copyright'], 
			'class' => '' 
		);
		
		break;
	}
	
	return apply_filters('cmsmasters_options_general_fields_filter', $options, $tab);
}

