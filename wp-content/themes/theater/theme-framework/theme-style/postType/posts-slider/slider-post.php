<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version		1.0.5
 * 
 * Posts Slider Post Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_metadata = explode(',', $cmsmasters_post_metadata);


$title = in_array('title', $cmsmasters_metadata) ? true : false;
$excerpt = (in_array('excerpt', $cmsmasters_metadata) && theater_slider_post_check_exc_cont('post')) ? true : false;
$date = in_array('date', $cmsmasters_metadata) ? true : false;
$categories = (get_the_category() && (in_array('categories', $cmsmasters_metadata))) ? true : false;
$author = in_array('author', $cmsmasters_metadata) ? true : false;
$comments = (comments_open() && (in_array('comments', $cmsmasters_metadata))) ? true : false;
$likes = in_array('likes', $cmsmasters_metadata) ? true : false;
$more = in_array('more', $cmsmasters_metadata) ? true : false;


$cmsmasters_post_format = get_post_format();

?>
<!-- Start Posts Slider Post Article -->
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_slider_post'); ?>>
	<div class="cmsmasters_slider_post_outer">
	<?php
		theater_thumb_rollover(get_the_ID(), 'cmsmasters-blog-masonry-thumb', false, false, false, false, false, false, false, false, true, false, false);
		
		
		
		if ($author || $categories || $likes || $comments || $date || $title || $excerpt || $more) {
			echo '<div class="cmsmasters_slider_post_inner">';
			
				if ($author || $categories || $date || $title) {
					echo '<div class="cmsmasters_slider_post_cont">';
						if ($author || $categories || $date) {
							echo '<div class="cmsmasters_slider_post_cont_info entry-meta">';
								$date ? theater_get_slider_post_date('post') : '';
								
								$author ? theater_get_slider_post_author('post') : '';
								
								$categories ? theater_get_slider_post_category(get_the_ID(), 'category', 'post') : '';
								
							echo '</div>';
						}
				
						$title ? theater_slider_post_heading(get_the_ID(), 'post', 'h4') : '';
					echo '</div>';
				}
				
				$excerpt ? theater_slider_post_exc_cont('post') : '';
				
				if ($likes || $comments || $more) {
					echo '<footer class="cmsmasters_slider_post_footer entry-meta">';
		
						$more ? theater_slider_post_more(get_the_ID()) : '';
						
						if ($likes || $comments) {
							echo '<div class="cmsmasters_slider_post_footer_info">';
								$likes ? theater_slider_post_like('post') : '';
						
								$comments ? theater_get_slider_post_comments('post') : '';
							echo '</div>';	
						}	
						
					echo '</footer>';
				}
				
				
				
			echo '</div>';
		}
		
		
	?>
	</div>
</article>
<!-- Finish Posts Slider Post Article -->

