<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version		1.0.0
 * 
 * TC Events Archives Events Template
 * Created by CMSMasters
 * 
 */


?>
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_tc_event'); ?>>
	<?php
	if (!post_password_required() && has_post_thumbnail()) {
		echo '<div class="cmsmasters_tc_event_img">';
			theater_thumb(get_the_ID(), 'post-thumbnail', true, false, true, false, true, true, false);
		echo '</div>';
	}
	
	
	echo '<div class="cmsmasters_tc_event_cont">';
		theater_tc_events_heading(get_the_ID(), 'h2');
		
		theater_tc_events_category(get_the_ID(), 'event_category');
		
		echo '<div class="cmsmasters_tc_event_info">';
			theater_tc_events_date();
			
			theater_tc_events_location();
		echo '</div>';
		
		theater_tc_events_content('page');
		
		theater_tc_events_ticket(get_the_ID());
	echo '</div>';
	?>
</article>