<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version		1.0.5
 * 
 * TC Events Single Event Template
 * Created by CMSMasters
 * 
 */


?>
<!-- Start TC Event Single Article -->
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_open_tc_event'); ?>>
	<?php
	echo '<div class="cmsmasters_tc_event_cont">' . 
		'<div class="cmsmasters_tc_event_header_wrap">';
			if (!post_password_required() && has_post_thumbnail()) {
				echo '<div class="cmsmasters_tc_event_img">';
					theater_thumb(get_the_ID(), 'cmsmasters-full-thumb', false, 'img_' . get_the_ID(), true, true, true, true, false);
				echo '</div>';
			}
			
			theater_tc_events_heading(get_the_ID(), 'h2', false);
			
			theater_tc_events_category(get_the_ID(), 'event_category');
			
			echo '<div class="cmsmasters_tc_event_info">';
				theater_tc_events_date();
				
				theater_tc_events_location();
			echo '</div>' . 
			
			theater_tc_events_content('post') . 
		'</div>';
	echo '</div>';
	
	theater_tc_events_ticket(get_the_ID());
	?>
</article>
<!-- Finish TC Event Single Article -->
<?php 

theater_prev_next_posts();

