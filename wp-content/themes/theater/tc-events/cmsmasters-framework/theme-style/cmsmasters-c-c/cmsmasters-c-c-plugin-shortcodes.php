<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version 	1.0.0
 * 
 * TC Events Content Composer Shortcodes
 * Created by CMSMasters
 * 
 */


function theater_tc_event_shortcodes($shortcodes) {
	$shortcodes[] = 'cmsmasters_tc_event_tickets';
	
	
	return $shortcodes;
}

add_filter('cmsmasters_custom_shortcodes_filter', 'theater_tc_event_shortcodes');


/**
 * Event Tickets
 */
function cmsmasters_tc_event_tickets($atts, $content = null) {
	extract(shortcode_atts(array( 
		'shortcode_id' => 		'', 
		'event' => 				'', 
		'link' => 				'', 
		'type' => 				'', 
		'price' => 				'', 
		'cart' => 				'', 
		'quantity' => 			'', 
		'soldout' => 			'', 
		'show_quantity' => 		'', 
		'link_type' => 			'', 
		'animation' => 			'', 
		'animation_delay' => 	'', 
		'classes' => 			'' 
	), $atts));
	
	
	$unique_id = $shortcode_id;
	
	
	$out = '<div id="cmsmasters_tc_event_tickets_' . $unique_id . '" class="cmsmasters_tc_event_tickets' . 
		(($classes != '') ? ' ' . $classes : '') . 
		'"' . 
		(($animation != '') ? ' data-animation="' . $animation . '"' : '') . 
		(($animation != '' && $animation_delay != '') ? ' data-delay="' . $animation_delay . '"' : '') . 
	'>' . "\n";
	
	
	$out .= do_shortcode(
		'[tc_event' . 
			' id="' . $event . '"' . 
			' title="' . $link . '"' . 
			' ticket_type_title="' . $type . '"' . 
			' price_title="' . $price . '"' . 
			' cart_title="' . $cart . '"' . 
			($show_quantity == 'true' ? ' quantity_title="' . $quantity . '"' : '') . 
			' soldout_message="' . $soldout . '"' . 
			($show_quantity == 'true' ? ' quantity="' . $show_quantity . '"' : '') . 
			' type="' . $link_type . '"' . 
		']'
	);
	
	
	$out .= '</div>';
	
	
	return $out;
}

