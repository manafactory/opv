<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version 	1.0.0
 * 
 * TC Events Content Composer Functions 
 * Created by CMSMasters
 * 
 */


/* Register JS Scripts */
function theater_tc_events_register_c_c_scripts() {
	global $pagenow;
	
	
	if ( 
		$pagenow == 'post-new.php' || 
		($pagenow == 'post.php' && isset($_GET['post']) && get_post_type($_GET['post']) != 'attachment') 
	) {
		wp_enqueue_script('theater-tc-events-extend', get_template_directory_uri() . '/tc-events/cmsmasters-framework/theme-style' . CMSMASTERS_THEME_STYLE . '/cmsmasters-c-c/js/cmsmasters-c-c-plugin-extend.js', array('cmsmasters_composer_shortcodes_js'), '1.0.0', true);
		
		wp_localize_script('theater-tc-events-extend', 'cmsmasters_tc_events_shortcodes', array( 
			'tc_event_ids' => 								theater_tc_events_event_ids(), 
			'tc_event_tickets_title' => 					esc_html__('Event Tickets', 'theater'), 
			'tc_event_tickets_field_event_title' => 		esc_html__('Event', 'theater'), 
			'tc_event_tickets_field_link_title' => 			esc_html__('Link Title', 'theater'), 
			'tc_event_tickets_field_link_def' => 			esc_html__('Add to Tickets Cart', 'theater'), 
			'tc_event_tickets_field_type_title' => 			esc_html__('Ticket Type Column Title', 'theater'), 
			'tc_event_tickets_field_type_def' => 			esc_html__('Ticket Type', 'theater'), 
			'tc_event_tickets_field_price_title' => 		esc_html__('Price Column Title', 'theater'), 
			'tc_event_tickets_field_price_def' => 			esc_html__('Price', 'theater'), 
			'tc_event_tickets_field_cart_title' => 			esc_html__('Cart Column Title', 'theater'), 
			'tc_event_tickets_field_cart_def' => 			esc_html__('Tickets Cart', 'theater'), 
			'tc_event_tickets_field_quantity_title' => 		esc_html__('Quantity Column Title', 'theater'), 
			'tc_event_tickets_field_quantity_def' => 		esc_html__('Qty.', 'theater'), 
			'tc_event_tickets_field_soldout_title' => 		esc_html__('Soldout Message', 'theater'), 
			'tc_event_tickets_field_soldout_def' => 		esc_html__('Tickets are sold out.', 'theater'), 
			'tc_event_tickets_field_show_quantity_title' => esc_html__('Show Quantity Selector', 'theater'), 
			'tc_event_tickets_field_show_quantity_no' => 	esc_html__('No', 'theater'), 
			'tc_event_tickets_field_show_quantity_yes' => 	esc_html__('Yes', 'theater'), 
			'tc_event_tickets_field_link_type_title' => 	esc_html__('Link Type', 'theater'), 
			'tc_event_tickets_field_link_type_descr' => 	esc_html__('If Buy Now is selected, after clicking on the link, user will be redirected automatically to the cart page.', 'theater'), 
			'tc_event_tickets_field_link_type_cart' => 		esc_html__('Cart', 'theater'), 
			'tc_event_tickets_field_link_type_buynow' => 	esc_html__('Buy Now', 'theater') 
		));
	}
}

add_action('admin_enqueue_scripts', 'theater_tc_events_register_c_c_scripts');


/* Event IDs */
function theater_tc_events_event_ids() {
	$cmsmasters_events_search = new TC_Events_Search('', '', -1);
	
	$cmsmasters_events = $cmsmasters_events_search->get_results();
	
	
	$out = array();
	
	
	if (!empty($cmsmasters_events)) {
		foreach ($cmsmasters_events as $event) {
			$event = new TC_Event($event->ID);
			
			$out[esc_attr($event->details->ID)] = esc_html($event->details->post_title);
		}
	}
	
	
	return $out;
}

