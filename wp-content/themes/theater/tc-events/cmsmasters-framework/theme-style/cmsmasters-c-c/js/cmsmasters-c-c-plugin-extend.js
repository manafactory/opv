/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version		1.0.0
 * 
 * TC Events Content Composer Schortcodes Extend
 * Created by CMSMasters
 * 
 */


/**
 * Event Tickets
 */
cmsmastersShortcodes.cmsmasters_tc_event_tickets = { 
	title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_title, 
	icon : 		'admin-icon-price', 
	pair : 		false, 
	content : 	false, 
	visual : 	false, 
	multiple : 	false, 
	def : 		'', 
	fields : { 
		// Shortcode ID
		shortcode_id : { 
			type : 		'hidden', 
			title : 	'', 
			descr : 	'', 
			def : 		'', 
			required : 	true, 
			width : 	'full' 
		}, 
		// Event
		event : { 
			type : 		'select', 
			title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_event_title, 
			descr : 	'', 
			def : 		'', 
			required : 	false, 
			width : 	'half', 
			choises : 	cmsmasters_tc_events_shortcodes.tc_event_ids 
		}, 
		// Link
		link : { 
			type : 		'input', 
			title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_link_title, 
			descr : 	'', 
			def : 		cmsmasters_tc_events_shortcodes.tc_event_tickets_field_link_def, 
			required : 	false, 
			width : 	'half' 
		}, 
		// Type
		type : { 
			type : 		'input', 
			title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_type_title, 
			descr : 	'', 
			def : 		cmsmasters_tc_events_shortcodes.tc_event_tickets_field_type_def, 
			required : 	false, 
			width : 	'half' 
		}, 
		// Price
		price : { 
			type : 		'input', 
			title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_price_title, 
			descr : 	'', 
			def : 		cmsmasters_tc_events_shortcodes.tc_event_tickets_field_price_def, 
			required : 	false, 
			width : 	'half' 
		}, 
		// Cart
		cart : { 
			type : 		'input', 
			title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_cart_title, 
			descr : 	'', 
			def : 		cmsmasters_tc_events_shortcodes.tc_event_tickets_field_cart_def, 
			required : 	false, 
			width : 	'half' 
		}, 
		// Quantity
		quantity : { 
			type : 		'input', 
			title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_quantity_title, 
			descr : 	'', 
			def : 		cmsmasters_tc_events_shortcodes.tc_event_tickets_field_quantity_def, 
			required : 	false, 
			width : 	'half' 
		}, 
		// Soldout
		soldout : { 
			type : 		'input', 
			title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_soldout_title, 
			descr : 	'', 
			def : 		cmsmasters_tc_events_shortcodes.tc_event_tickets_field_soldout_def, 
			required : 	false, 
			width : 	'half' 
		}, 
		// Show Quantity
		show_quantity : { 
			type : 		'select', 
			title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_show_quantity_title, 
			descr : 	'', 
			def : 		'false', 
			required : 	false, 
			width : 	'half', 
			choises : { 
						'false' : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_show_quantity_no, 
						'true' : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_show_quantity_yes 
			} 
		}, 
		// Link Type
		link_type : { 
			type : 		'select', 
			title : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_link_type_title, 
			descr : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_link_type_descr, 
			def : 		'cart', 
			required : 	false, 
			width : 	'half', 
			choises : { 
						'cart' : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_link_type_cart, 
						'buynow' : 	cmsmasters_tc_events_shortcodes.tc_event_tickets_field_link_type_buynow 
			} 
		}, 
		// CSS3 Animation
		animation : { 
			type : 		'select', 
			title : 	cmsmasters_shortcodes.animation_title, 
			descr : 	cmsmasters_shortcodes.animation_descr + " <br /><span>" + cmsmasters_shortcodes.note + ' ' + cmsmasters_shortcodes.animation_descr_note + "</span>", 
			def : 		'', 
			required : 	false, 
			width : 	'half', 
			choises : 	get_animations() 
		}, 
		// Animation Delay
		animation_delay : { 
			type : 		'input', 
			title : 	cmsmasters_shortcodes.animation_delay_title, 
			descr : 	cmsmasters_shortcodes.animation_delay_descr + " <br /><span>" + cmsmasters_shortcodes.note + ' ' + cmsmasters_shortcodes.animation_delay_descr_note + "</span>", 
			def : 		'0', 
			required : 	false, 
			width : 	'number', 
			min : 		'0', 
			step : 		'50' 
		}, 
		// Additional Classes
		classes : { 
			type : 		'input', 
			title : 	cmsmasters_shortcodes.classes_title, 
			descr : 	cmsmasters_shortcodes.classes_descr, 
			def : 		'', 
			required : 	false, 
			width : 	'half' 
		} 
	} 
};

