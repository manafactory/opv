<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version 	1.0.0
 * 
 * TC Events Fonts Rules
 * Created by CMSMasters
 * 
 */


function theater_tc_events_fonts($custom_css) {
	$cmsmasters_option = theater_get_global_options();
	
	
	$custom_css .= "
/***************** Start TC Events Font Styles ******************/

	/* Start Content Font */
	.tc_event_content {
		font-family:" . theater_get_google_font($cmsmasters_option['theater' . '_content_font_google_font']) . $cmsmasters_option['theater' . '_content_font_system_font'] . ";
		font-size:" . $cmsmasters_option['theater' . '_content_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['theater' . '_content_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['theater' . '_content_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['theater' . '_content_font_font_style'] . ";
	}
	
	.cmsmasters_tc_event .cmsmasters_tc_event_info span, 
	.cmsmasters_open_tc_event .cmsmasters_tc_event_info span {
		font-size:" . ((int) $cmsmasters_option['theater' . '_content_font_font_size'] - 2) . "px;
	}
	/* Finish Content Font */
	
	
	/* Start H1 Font */
	/* Finish H1 Font */
	
	
	/* Start H2 Font */
	.cmsmasters_open_tc_event .cmsmasters_tc_event_title {
		font-size:" . ((int)$cmsmasters_option['theater' . '_h2_font_font_size'] + 10) . "px;
		line-height:" . ((int)$cmsmasters_option['theater' . '_h2_font_line_height'] + 10) . "px;
	}
	
	.cmsmasters_tc_event .cmsmasters_tc_event_title, 
	.cmsmasters_tc_event .cmsmasters_tc_event_title a {
		text-transform:none;
	}
	/* Finish H2 Font */
	
	
	/* Start H3 Font */ 
	.tickera_owner_info > h2,
	.event_tickets td:first-child,
	.tickera_table td:first-child,
	.event_tickets a,
	.event_tickets .tc_in_cart {
		font-family:" . theater_get_google_font($cmsmasters_option['theater' . '_h3_font_google_font']) . $cmsmasters_option['theater' . '_h3_font_system_font'] . ";
		font-size:" . $cmsmasters_option['theater' . '_h3_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['theater' . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['theater' . '_h3_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['theater' . '_h3_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['theater' . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['theater' . '_h3_font_text_decoration'] . ";
	}
	
	.event_tickets td:first-child,
	.tickera_table td:first-child,
	.event_tickets a,
	.event_tickets .tc_in_cart {
		font-size:" . ((int)$cmsmasters_option['theater' . '_h3_font_font_size'] - 4) . "px;
	}
	/* Finish H3 Font */
	
	
	/* Start H4 Font */
	.event_tickets th, 
	.tickera_table th, 
	.tickera .order-details th,
	.tickera-payment-gateways .plugin-title {
		font-family:" . theater_get_google_font($cmsmasters_option['theater' . '_h4_font_google_font']) . $cmsmasters_option['theater' . '_h4_font_system_font'] . ";
		font-size:" . $cmsmasters_option['theater' . '_h4_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['theater' . '_h4_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['theater' . '_h4_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['theater' . '_h4_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['theater' . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['theater' . '_h4_font_text_decoration'] . ";
	}
	/* Finish H4 Font */
	
	
	/* Start H5 Font */
	.event_tickets, 
	.tickera_table, 
	.tickera > p, 
	.tickera > label, 
	.tickera-payment-gateways .tc_redirect_message, 
	.tc_cart_widget .tc_cart_ul li {
		font-family:" . theater_get_google_font($cmsmasters_option['theater' . '_h5_font_google_font']) . $cmsmasters_option['theater' . '_h5_font_system_font'] . ";
		font-size:" . $cmsmasters_option['theater' . '_h5_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['theater' . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['theater' . '_h5_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['theater' . '_h5_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['theater' . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['theater' . '_h5_font_text_decoration'] . ";
	}
	
	.event_tickets, 
	.tickera_table, 
	.tickera > p, 
	.tickera > label {
		font-size:" . ((int) $cmsmasters_option['theater' . '_h5_font_font_size'] + 2) . "px;
	}
	/* Finish H5 Font */
	
	
	/* Start H6 Font */
	.cmsmasters_tc_event .cmsmasters_tc_event_category a, 
	.cmsmasters_open_tc_event .cmsmasters_tc_event_category a {
		font-family:" . theater_get_google_font($cmsmasters_option['theater' . '_h6_font_google_font']) . $cmsmasters_option['theater' . '_h6_font_system_font'] . ";
		font-size:" . $cmsmasters_option['theater' . '_h6_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['theater' . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['theater' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['theater' . '_h6_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['theater' . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['theater' . '_h6_font_text_decoration'] . ";
	}
	/* Finish H6 Font */
	
	
	/* Start Button Font */
	.tc_event_button {
		font-family:" . theater_get_google_font($cmsmasters_option['theater' . '_button_font_google_font']) . $cmsmasters_option['theater' . '_button_font_system_font'] . ";
		font-size:" . $cmsmasters_option['theater' . '_button_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['theater' . '_button_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['theater' . '_button_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['theater' . '_button_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['theater' . '_button_font_text_transform'] . ";
	}
	/* Finish Button Font */
	
	
	/* Start Small Text Font */
	.tc_event_small	{
		font-family:" . theater_get_google_font($cmsmasters_option['theater' . '_small_font_google_font']) . $cmsmasters_option['theater' . '_small_font_system_font'] . ";
		font-size:" . $cmsmasters_option['theater' . '_small_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['theater' . '_small_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['theater' . '_small_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['theater' . '_small_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['theater' . '_small_font_text_transform'] . ";
	}
	/* Finish Small Text Font */

/***************** Finish TC Events Font Styles ******************/

";
	
	
	return $custom_css;
}

add_filter('theater_theme_fonts_filter', 'theater_tc_events_fonts');

