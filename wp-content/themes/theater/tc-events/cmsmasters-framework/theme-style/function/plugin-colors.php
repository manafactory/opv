<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version 	1.0.0
 * 
 * TC Events Colors Rules
 * Created by CMSMasters
 * 
 */


function theater_tc_events_colors($custom_css) {
	$cmsmasters_option = theater_get_global_options();
	
	
	$cmsmasters_color_schemes = cmsmasters_color_schemes_list();
	
	
	foreach ($cmsmasters_color_schemes as $scheme => $title) {
		$rule = (($scheme != 'default') ? "html .cmsmasters_color_scheme_{$scheme} " : '');
		
		
		$custom_css .= "
/***************** Start {$title} TC Events Color Scheme Rules ******************/

	/* Start Main Content Font Color */
	{$rule}.tc_event_color {
		" . cmsmasters_color_css('color', $cmsmasters_option['theater' . '_' . $scheme . '_color']) . "
	}
	/* Finish Main Content Font Color */
	
	
	/* Start Primary Color */
	{$rule}.cmsmasters_tc_event .cmsmasters_tc_event_title a:hover, 
	{$rule}.cmsmasters_tc_event .cmsmasters_tc_event_category a, 
	{$rule}.cmsmasters_open_tc_event .cmsmasters_tc_event_category a {
		" . cmsmasters_color_css('color', $cmsmasters_option['theater' . '_' . $scheme . '_link']) . "
	}
	/* Finish Primary Color */
	
	
	/* Start Highlight Color */
	{$rule}.tickera_table .ticket-total-all *,
	{$rule}.fields-wrap label span,
	{$rule}.cmsmasters_tc_event .cmsmasters_tc_event_category a:hover, 
	{$rule}.cmsmasters_open_tc_event .cmsmasters_tc_event_category a:hover, 
	{$rule}.tickera_table .ticket-quantity .minus:hover, 
	{$rule}.tickera_table .ticket-quantity .plus:hover {
		" . cmsmasters_color_css('color', $cmsmasters_option['theater' . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.event_tickets th, 
	{$rule}.tickera_table th {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['theater' . '_' . $scheme . '_hover']) . "
	}
	/* Finish Highlight Color */
	
	
	/* Start Headings Color */
	{$rule}.tc_cart_errors > ul, 
	{$rule}.event_tickets, 
	{$rule}.event_tickets select, 
	{$rule}.tickera_table, 
	{$rule}.tickera_table .ticket-total-all *:nth-child(1), 
	{$rule}.tickera_table .ticket-total-all *:nth-child(2), 
	{$rule}.tickera_table .ticket-total-all .cart_total_price_title, 
	{$rule}.tickera_table .ticket-total-all .cart_total_price, 
	{$rule}.cmsmasters_tc_event .cmsmasters_tc_event_info span, 
	{$rule}.cmsmasters_open_tc_event .cmsmasters_tc_event_info span, 
	{$rule}.tickera_table .ticket-quantity input, 
	{$rule}.tickera-payment-gateways .plugin-title, 
	{$rule}.tickera > p, 
	{$rule}.tickera > label, 
	{$rule}.tickera-payment-gateways .tc_redirect_message, 
	{$rule}.tc_cart_widget .tc_cart_ul li {
		" . cmsmasters_color_css('color', $cmsmasters_option['theater' . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.event_tickets th, 
	{$rule}.tickera_table th {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['theater' . '_' . $scheme . '_heading']) . "
	}
	/* Finish Headings Color */
	
	
	/* Start Main Background Color */
	{$rule}.tc_event_bg {
		" . cmsmasters_color_css('color', $cmsmasters_option['theater' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.event_tickets select, 
	{$rule}.tickera_table .ticket-quantity input:not([type=submit]):not([type=button]):not([type=radio]):not([type=checkbox]), 
	{$rule}.event_tickets td, 
	{$rule}.tickera_table td {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['theater' . '_' . $scheme . '_bg']) . "
	}
	/* Finish Main Background Color */
	
	
	/* Start Alternate Background Color */
	{$rule}.event_tickets th, 
	{$rule}.tickera_table th {
		" . cmsmasters_color_css('color', $cmsmasters_option['theater' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.tc_cart_errors > ul, 
	{$rule}.tickera > p, 
	{$rule}.tickera > label, 
	{$rule}.tickera > hr, 
	{$rule}.tickera .order-details th, 
	{$rule}#tc_payment_form, 
	{$rule}.tc_cart_widget .tc_cart_ul li {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['theater' . '_' . $scheme . '_alternate']) . "
	}
	/* Finish Alternate Background Color */
	
	
	/* Start Borders Color */
	{$rule}.cmsmasters_tc_event, 
	{$rule}.tc_cart_errors > ul, 
	{$rule}.event_tickets td, 
	{$rule}.tickera_table td, 
	{$rule}.cmsmasters_tc_event .cmsmasters_tc_event_cont, 
	{$rule}.cmsmasters_open_tc_event .cmsmasters_tc_event_cont, 
	{$rule}.cmsmasters_open_tc_event .cmsmasters_tc_event_img, 
	{$rule}.cmsmasters_open_tc_event .cmsmasters_tc_event_content, 
	{$rule}.tickera > p, 
	{$rule}.tickera > label, 
	{$rule}.tickera > hr, 
	{$rule}.tickera .order-details th, 
	{$rule}.tickera .order-details td, 
	{$rule}#tc_payment_form, 
	{$rule}.tc_cart_widget .tc_cart_ul li {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['theater' . '_' . $scheme . '_border']) . "
	}
	/* Finish Borders Color */

/***************** Finish {$title} TC Events Color Scheme Rules ******************/

";
	}
	
	
	return $custom_css;
}

add_filter('theater_theme_colors_secondary_filter', 'theater_tc_events_colors');

