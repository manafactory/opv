<?php 
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version		1.0.9
 * 
 * Theme Settings Defaults
 * Created by CMSMasters
 * 
 */


/* Theme Settings General Default Values */
if (!function_exists('theater_settings_general_defaults')) {

function theater_settings_general_defaults($id = false) {
	$settings = array( 
		'general' => array( 
			'theater' . '_theme_layout' => 		'liquid', 
			'theater' . '_logo_type' => 			'image', 
			'theater' . '_logo_url' => 			'|' . get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_THEME_STYLE . '/img/logo.png', 
			'theater' . '_logo_url_retina' => 		'|' . get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_THEME_STYLE . '/img/logo_retina.png', 
			'theater' . '_logo_title' => 			get_bloginfo('name') ? get_bloginfo('name') : 'Theater', 
			'theater' . '_logo_subtitle' => 		'', 
			'theater' . '_logo_custom_color' => 	0, 
			'theater' . '_logo_title_color' => 	'', 
			'theater' . '_logo_subtitle_color' => 	'' 
		), 
		'bg' => array( 
			'theater' . '_bg_col' => 			'#ffffff', 
			'theater' . '_bg_img_enable' => 	0, 
			'theater' . '_bg_img' => 			'', 
			'theater' . '_bg_rep' => 			'no-repeat', 
			'theater' . '_bg_pos' => 			'top center', 
			'theater' . '_bg_att' => 			'scroll', 
			'theater' . '_bg_size' => 			'cover' 
		), 
		'header' => array( 
			'theater' . '_fixed_header' => 				1, 
			'theater' . '_header_overlaps' => 				1, 
			'theater' . '_header_top_line' => 				0, 
			'theater' . '_header_top_height' => 			'32', 
			'theater' . '_header_top_line_short_info' => 	'', 
			'theater' . '_header_top_line_add_cont' => 	'social', 
			'theater' . '_header_styles' => 				'fullwidth', 
			'theater' . '_header_mid_height' => 			'100', 
			'theater' . '_header_bot_height' => 			'60', 
			'theater' . '_header_search' => 				1, 
			'theater' . '_header_add_cont' => 				'none', 
			'theater' . '_header_add_cont_cust_html' => 	'' 
		), 
		'content' => array( 
			'theater' . '_layout' => 					'fullwidth', 
			'theater' . '_archives_layout' => 			'fullwidth', 
			'theater' . '_search_layout' => 			'r_sidebar', 
			'theater' . '_other_layout' => 			'r_sidebar', 
			'theater' . '_heading_alignment' => 		'center', 
			'theater' . '_heading_scheme' => 			'default', 
			'theater' . '_heading_bg_image_enable' => 	1, 
			'theater' . '_heading_bg_image' => 		'|' . get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_THEME_STYLE . '/img/heading.jpg', 
			'theater' . '_heading_bg_repeat' => 		'no-repeat', 
			'theater' . '_heading_bg_attachment' => 	'scroll', 
			'theater' . '_heading_bg_size' => 			'cover', 
			'theater' . '_heading_bg_color' => 		'', 
			'theater' . '_heading_height' => 			'315', 
			'theater' . '_breadcrumbs' => 				1, 
			'theater' . '_bottom_scheme' => 			'second', 
			'theater' . '_bottom_sidebar' => 			1, 
			'theater' . '_bottom_sidebar_layout' => 	'14141414' 
		), 
		'footer' => array( 
			'theater' . '_footer_scheme' => 				'footer', 
			'theater' . '_footer_type' => 					'default', 
			'theater' . '_footer_additional_content' => 	'social', 
			'theater' . '_footer_logo' => 					1, 
			'theater' . '_footer_logo_url' => 				'|' . get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_THEME_STYLE . '/img/logo_footer.png', 
			'theater' . '_footer_logo_url_retina' => 		'|' . get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_THEME_STYLE . '/img/logo_footer_retina.png', 
			'theater' . '_footer_nav' => 					0, 
			'theater' . '_footer_social' => 				1, 
			'theater' . '_footer_html' => 					'', 
			'theater' . '_footer_copyright' => 			'&copy; ' . date('Y') . ' Theater' 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



/* Theme Settings Fonts Default Values */
if (!function_exists('theater_settings_font_defaults')) {

function theater_settings_font_defaults($id = false) {
	$settings = array( 
		'content' => array( 
			'theater' . '_content_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Open+Sans:300,300italic,400,400italic,700,700italic', 
				'font_size' => 			'14', 
				'line_height' => 		'24', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal' 
			) 
		), 
		'link' => array( 
			'theater' . '_link_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Open+Sans:300,300italic,400,400italic,700,700italic', 
				'font_size' => 			'14', 
				'line_height' => 		'24', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'theater' . '_link_hover_decoration' => 	'none' 
		), 
		'nav' => array( 
			'theater' . '_nav_title_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Open+Sans:300,300italic,400,400italic,700,700italic', 
				'font_size' => 			'14', 
				'line_height' => 		'20', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'uppercase' 
			), 
			'theater' . '_nav_dropdown_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Open+Sans:300,300italic,400,400italic,700,700italic', 
				'font_size' => 			'14', 
				'line_height' => 		'20', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none' 
			) 
		), 
		'heading' => array( 
			'theater' . '_h1_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Cormorant+Garamond:400,700,700italic', 
				'font_size' => 			'80', 
				'line_height' => 		'105', 
				'font_weight' => 		'bold', 
				'font_style' => 		'italic', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'theater' . '_h2_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Cormorant+Garamond:400,700,700italic', 
				'font_size' => 			'30', 
				'line_height' => 		'40', 
				'font_weight' => 		'bold', 
				'font_style' => 		'italic', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'theater' . '_h3_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Cormorant+Garamond:400,700,700italic', 
				'font_size' => 			'24', 
				'line_height' => 		'34', 
				'font_weight' => 		'bold', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'theater' . '_h4_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Cormorant+Garamond:400,700,700italic', 
				'font_size' => 			'22', 
				'line_height' => 		'32', 
				'font_weight' => 		'bold', 
				'font_style' => 		'italic', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'theater' . '_h5_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Cormorant+Garamond:400,700,700italic', 
				'font_size' => 			'18', 
				'line_height' => 		'30', 
				'font_weight' => 		'bold', 
				'font_style' => 		'italic', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'theater' . '_h6_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Open+Sans:300,300italic,400,400italic,700,700italic', 
				'font_size' => 			'14', 
				'line_height' => 		'28', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			) 
		), 
		'other' => array( 
			'theater' . '_button_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Open+Sans:300,300italic,400,400italic,700,700italic', 
				'font_size' => 			'13', 
				'line_height' => 		'46', 
				'font_weight' => 		'500', 
				'font_style' => 		'normal', 
				'text_transform' => 	'uppercase' 
			), 
			'theater' . '_small_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Open+Sans:300,300italic,400,400italic,700,700italic', 
				'font_size' => 			'14', 
				'line_height' => 		'24', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none' 
			), 
			'theater' . '_input_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Open+Sans:300,300italic,400,400italic,700,700italic', 
				'font_size' => 			'14', 
				'line_height' => 		'24', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal' 
			), 
			'theater' . '_quote_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Cormorant+Garamond:400,700,700italic', 
				'font_size' => 			'40', 
				'line_height' => 		'56', 
				'font_weight' => 		'bold', 
				'font_style' => 		'italic' 
			) 
		),
		'google' => array( 
			'theater' . '_google_web_fonts' => array( 
				'Cormorant+Garamond:400,700,700italic|Cormorant Garamond', 
				'Roboto:300,300italic,400,400italic,500,500italic,700,700italic|Roboto', 
				'Roboto+Condensed:400,400italic,700,700italic|Roboto Condensed', 
				'Open+Sans:300,300italic,400,400italic,700,700italic|Open Sans', 
				'Open+Sans+Condensed:300,300italic,700|Open Sans Condensed', 
				'Droid+Sans:400,700|Droid Sans', 
				'Droid+Serif:400,400italic,700,700italic|Droid Serif', 
				'PT+Sans:400,400italic,700,700italic|PT Sans', 
				'PT+Sans+Caption:400,700|PT Sans Caption', 
				'PT+Sans+Narrow:400,700|PT Sans Narrow', 
				'PT+Serif:400,400italic,700,700italic|PT Serif', 
				'Ubuntu:400,400italic,700,700italic|Ubuntu', 
				'Ubuntu+Condensed|Ubuntu Condensed', 
				'Headland+One|Headland One', 
				'Source+Sans+Pro:300,300italic,400,400italic,700,700italic|Source Sans Pro', 
				'Lato:400,400italic,700,700italic|Lato', 
				'Cuprum:400,400italic,700,700italic|Cuprum', 
				'Oswald:300,400,700|Oswald', 
				'Yanone+Kaffeesatz:300,400,700|Yanone Kaffeesatz', 
				'Lobster|Lobster', 
				'Lobster+Two:400,400italic,700,700italic|Lobster Two', 
				'Questrial|Questrial', 
				'Raleway:300,400,500,600,700|Raleway', 
				'Dosis:300,400,500,700|Dosis', 
				'Cutive+Mono|Cutive Mono', 
				'Quicksand:300,400,700|Quicksand', 
				'Montserrat:400,700|Montserrat', 
				'Cookie|Cookie' 
			) 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



// WP Color Picker Palettes
if (!function_exists('cmsmasters_color_picker_palettes')) {

function cmsmasters_color_picker_palettes() {
	$palettes = array( 
		'#000000', 
		'#ffffff', 
		'#d43c18', 
		'#5173a6', 
		'#959595', 
		'#c0c0c0', 
		'#f4f4f4', 
		'#e1e1e1' 
	);
	
	
	return $palettes;
}

}



// Theme Settings Color Schemes Default Colors
if (!function_exists('theater_color_schemes_defaults')) {

function theater_color_schemes_defaults($id = false) {
	$settings = array( 
		'default' => array( // content default color scheme
			'color' => 		'#444444', 
			'link' => 		'#b49761', 
			'hover' => 		'#a49f95', 
			'heading' => 	'#000000', 
			'bg' => 		'#ffffff', 
			'alternate' => 	'#f7f7f7', 
			'border' => 	'#dedede' 
		), 
		'header' => array( // Header color scheme
			'mid_color' => 		'#ffffff', 
			'mid_link' => 		'#ffffff', 
			'mid_hover' => 		'rgba(255,255,255,0.5)', 
			'mid_bg' => 		'rgba(49,51,59,0.15)', 
			'mid_bg_scroll' => 	'#000000', 
			'mid_border' => 	'rgba(255,255,255,0.2)', 
			'bot_color' => 		'#ffffff', 
			'bot_link' => 		'#ffffff', 
			'bot_hover' => 		'#ffffff', 
			'bot_bg' => 		'rgba(49,51,59,0.15)', 
			'bot_bg_scroll' => 	'#ffffff', 
			'bot_border' => 	'rgba(255,255,255,0.2)' 
		), 
		'navigation' => array( // Navigation color scheme
			'title_link' => 			'#ffffff', 
			'title_link_hover' => 		'rgba(255,255,255,0.65)', 
			'title_link_current' => 	'rgba(255,255,255,0.8)', 
			'title_link_subtitle' => 	'rgba(255,255,255,0.3)', 
			'title_link_bg' => 			'rgba(255,255,255,0)', 
			'title_link_bg_hover' => 	'rgba(255,255,255,0)', 
			'title_link_bg_current' => 	'rgba(255,255,255,0)', 
			'title_link_border' => 		'rgba(255,255,255,0)', 
			'dropdown_text' => 			'#717275', 
			'dropdown_bg' => 			'#31333b', 
			'dropdown_border' => 		'rgba(255,255,255,0)', 
			'dropdown_link' => 			'#8a8a8e', 
			'dropdown_link_hover' => 	'#ffffff', 
			'dropdown_link_subtitle' => '#717275', 
			'dropdown_link_highlight' => 'rgba(255,255,255,0)', 
			'dropdown_link_border' => 	'rgba(255,255,255,0)' 
		), 
		'header_top' => array( // Header Top color scheme
			'color' => 					'#ffffff', 
			'link' => 					'#ffffff', 
			'hover' => 					'rgba(255,255,255,0.5)', 
			'bg' => 					'rgba(49,51,59,0.15)', 
			'border' => 				'rgba(255,255,255,0.2)', 
			'title_link' => 			'#ffffff', 
			'title_link_hover' => 		'rgba(255,255,255,0.5)', 
			'title_link_bg' => 			'rgba(255,255,255,0)', 
			'title_link_bg_hover' => 	'rgba(255,255,255,0)', 
			'title_link_border' => 		'rgba(255,255,255,0)', 
			'dropdown_bg' => 			'#31333b', 
			'dropdown_border' => 		'rgba(255,255,255,0)', 
			'dropdown_link' => 			'#8a8a8e', 
			'dropdown_link_hover' => 	'#ffffff', 
			'dropdown_link_highlight' => 'rgba(255,255,255,0)', 
			'dropdown_link_border' => 	'rgba(255,255,255,0)' 
		), 
		'footer' => array( // Footer color scheme
			'color' => 		'rgba(255,255,255,0.4)', 
			'link' => 		'#b49761', 
			'hover' => 		'#a49f95', 
			'heading' => 	'#ffffff', 
			'bg' => 		'#000000', 
			'alternate' => 	'#f7f7f7', 
			'border' => 	'rgba(255,255,255,0.1)' 
		), 
		'first' => array( // custom color scheme 1
			'color' => 		'#595959', 
			'link' => 		'#b49761', 
			'hover' => 		'#595959', 
			'heading' => 	'#a49f95', 
			'bg' => 		'#ffffff', 
			'alternate' => 	'#262626', 
			'border' => 	'#dedede' 
		), 
		'second' => array( // custom color scheme 2
			'color' => 		'rgba(255,255,255,0.4)', 
			'link' => 		'rgba(255,255,255,0.4)', 
			'hover' => 		'#b49761', 
			'heading' => 	'#ffffff', 
			'bg' => 		'#000000', 
			'alternate' => 	'rgba(255,255,255,0)', 
			'border' => 	'rgba(255,255,255,0.15)' 
		), 
		'third' => array( // custom color scheme 3
			'color' => 		'rgba(255,255,255,0.4)', 
			'link' => 		'#b49761', 
			'hover' => 		'rgba(255,255,255,0.5)', 
			'heading' => 	'#ffffff', 
			'bg' => 		'#000000', 
			'alternate' => 	'#f7f7f7', 
			'border' => 	'#dedede' 
		), 
		'fourth' => array( // custom color scheme 4
			'color' => 		'#ffffff', 
			'link' => 		'#ffffff', 
			'hover' => 		'#ffffff', 
			'heading' => 	'#ffffff', 
			'bg' => 		'#ffffff', 
			'alternate' => 	'#f7f7f7', 
			'border' => 	'rgba(255,255,255,0.5)' 
		)  
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



// Theme Settings Elements Default Values
if (!function_exists('theater_settings_element_defaults')) {

function theater_settings_element_defaults($id = false) {
	$settings = array( 
		'sidebar' => array( 
			'theater' . '_sidebar' => 	'' 
		), 
		'icon' => array( 
			'theater' . '_social_icons' => array( 
				'cmsmasters-icon-linkedin|#|' . esc_html__('Linkedin', 'theater') . '|true||', 
				'cmsmasters-icon-facebook|#|' . esc_html__('Facebook', 'theater') . '|true||', 
				'cmsmasters-icon-gplus|#|' . esc_html__('Google', 'theater') . '|true||', 
				'cmsmasters-icon-twitter|#|' . esc_html__('Twitter', 'theater') . '|true||', 
				'cmsmasters-icon-skype|#|' . esc_html__('Skype', 'theater') . '|true||' 
			) 
		), 
		'lightbox' => array( 
			'theater' . '_ilightbox_skin' => 					'dark', 
			'theater' . '_ilightbox_path' => 					'vertical', 
			'theater' . '_ilightbox_infinite' => 				0, 
			'theater' . '_ilightbox_aspect_ratio' => 			1, 
			'theater' . '_ilightbox_mobile_optimizer' => 		1, 
			'theater' . '_ilightbox_max_scale' => 				1, 
			'theater' . '_ilightbox_min_scale' => 				0.2, 
			'theater' . '_ilightbox_inner_toolbar' => 			0, 
			'theater' . '_ilightbox_smart_recognition' => 		0, 
			'theater' . '_ilightbox_fullscreen_one_slide' => 	0, 
			'theater' . '_ilightbox_fullscreen_viewport' => 	'center', 
			'theater' . '_ilightbox_controls_toolbar' => 		1, 
			'theater' . '_ilightbox_controls_arrows' => 		0, 
			'theater' . '_ilightbox_controls_fullscreen' => 	1, 
			'theater' . '_ilightbox_controls_thumbnail' => 	1, 
			'theater' . '_ilightbox_controls_keyboard' => 		1, 
			'theater' . '_ilightbox_controls_mousewheel' => 	1, 
			'theater' . '_ilightbox_controls_swipe' => 		1, 
			'theater' . '_ilightbox_controls_slideshow' => 	0 
		), 
		'sitemap' => array( 
			'theater' . '_sitemap_nav' => 			1, 
			'theater' . '_sitemap_categs' => 		1, 
			'theater' . '_sitemap_tags' => 		1, 
			'theater' . '_sitemap_month' => 		1, 
			'theater' . '_sitemap_pj_categs' => 	1, 
			'theater' . '_sitemap_pj_tags' => 		1 
		), 
		'error' => array( 
			'theater' . '_error_color' => 				'#292929', 
			'theater' . '_error_bg_color' => 			'#fcfcfc', 
			'theater' . '_error_bg_img_enable' => 		0, 
			'theater' . '_error_bg_image' => 			'', 
			'theater' . '_error_bg_rep' => 			'no-repeat', 
			'theater' . '_error_bg_pos' => 			'top center', 
			'theater' . '_error_bg_att' => 			'scroll', 
			'theater' . '_error_bg_size' => 			'cover', 
			'theater' . '_error_search' => 			1, 
			'theater' . '_error_sitemap_button' => 	1, 
			'theater' . '_error_sitemap_link' => 		'' 
		), 
		'code' => array( 
			'theater' . '_custom_css' => 			'', 
			'theater' . '_custom_js' => 			'', 
			'theater' . '_gmap_api_key' => 		'', 
			'theater' . '_api_key' => 				'', 
			'theater' . '_api_secret' => 			'', 
			'theater' . '_access_token' => 		'', 
			'theater' . '_access_token_secret' => 	'' 
		), 
		'recaptcha' => array( 
			'theater' . '_recaptcha_public_key' => 	'', 
			'theater' . '_recaptcha_private_key' => 	'' 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



// Theme Settings Single Posts Default Values
if (!function_exists('theater_settings_single_defaults')) {

function theater_settings_single_defaults($id = false) {
	$settings = array( 
		'post' => array( 
			'theater' . '_blog_post_layout' => 		'r_sidebar', 
			'theater' . '_blog_post_title' => 			1, 
			'theater' . '_blog_post_date' => 			1, 
			'theater' . '_blog_post_cat' => 			1, 
			'theater' . '_blog_post_author' => 		1, 
			'theater' . '_blog_post_comment' => 		1, 
			'theater' . '_blog_post_tag' => 			1, 
			'theater' . '_blog_post_like' => 			1, 
			'theater' . '_blog_post_nav_box' => 		1, 
			'theater' . '_blog_post_nav_order_cat' => 		0, 
			'theater' . '_blog_post_share_box' => 		1, 
			'theater' . '_blog_post_author_box' => 	1, 
			'theater' . '_blog_more_posts_box' => 		'popular', 
			'theater' . '_blog_more_posts_count' => 	'3', 
			'theater' . '_blog_more_posts_pause' => 	'5' 
		), 
		'project' => array( 
			'theater' . '_portfolio_project_title' => 			1, 
			'theater' . '_portfolio_project_details_title' => 	esc_html__('Project details', 'theater'), 
			'theater' . '_portfolio_project_date' => 			1, 
			'theater' . '_portfolio_project_cat' => 			1, 
			'theater' . '_portfolio_project_author' => 		1, 
			'theater' . '_portfolio_project_comment' => 		1, 
			'theater' . '_portfolio_project_tag' => 			0, 
			'theater' . '_portfolio_project_like' => 			1, 
			'theater' . '_portfolio_project_link' => 			0, 
			'theater' . '_portfolio_project_share_box' => 		1, 
			'theater' . '_portfolio_project_nav_box' => 		1, 
			'theater' . '_portfolio_project_nav_order_cat' => 		0, 
			'theater' . '_portfolio_project_author_box' => 	0, 
			'theater' . '_portfolio_more_projects_box' => 		'popular', 
			'theater' . '_portfolio_more_projects_count' => 	'4', 
			'theater' . '_portfolio_more_projects_pause' => 	'5', 
			'theater' . '_portfolio_project_slug' => 			'project', 
			'theater' . '_portfolio_pj_categs_slug' => 		'pj-categs', 
			'theater' . '_portfolio_pj_tags_slug' => 			'pj-tags' 
		), 
		'profile' => array( 
			'theater' . '_profile_post_title' => 			1, 
			'theater' . '_profile_post_details_title' => 	esc_html__('Profile details', 'theater'), 
			'theater' . '_profile_post_cat' => 			1, 
			'theater' . '_profile_post_comment' => 		1, 
			'theater' . '_profile_post_like' => 			1, 
			'theater' . '_profile_post_nav_box' => 		1, 
			'theater' . '_profile_post_nav_order_cat' => 		0, 
			'theater' . '_profile_post_share_box' => 		0, 
			'theater' . '_profile_post_slug' => 			'profile', 
			'theater' . '_profile_pl_categs_slug' => 		'pl-categs' 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



/* Project Puzzle Proportion */
if (!function_exists('theater_project_puzzle_proportion')) {

function theater_project_puzzle_proportion() {
	return 0.75;
}

}



/* Theme Image Thumbnails Size */
if (!function_exists('theater_get_image_thumbnail_list')) {

function theater_get_image_thumbnail_list() {
	$list = array( 
		'cmsmasters-small-thumb' => array( 
			'width' => 		70, 
			'height' => 	70, 
			'crop' => 		true 
		), 
		'cmsmasters-square-thumb' => array( 
			'width' => 		300, 
			'height' => 	300, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Square', 'theater') 
		), 
		'cmsmasters-blog-masonry-thumb' => array( 
			'width' => 		360, 
			'height' => 	360, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Masonry Blog', 'theater') 
		), 
		'cmsmasters-project-grid-thumb' => array( 
			'width' => 		560, 
			'height' => 	340, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Project Grid', 'theater') 
		), 
		'cmsmasters-project-thumb' => array( 
			'width' => 		580, 
			'height' => 	580, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Project', 'theater') 
		), 
		'cmsmasters-project-masonry-thumb' => array( 
			'width' => 		580, 
			'height' => 	9999, 
			'title' => 		esc_attr__('Masonry Project', 'theater') 
		), 
		'post-thumbnail' => array( 
			'width' => 		860, 
			'height' => 	500, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Featured', 'theater') 
		), 
		'cmsmasters-masonry-thumb' => array( 
			'width' => 		860, 
			'height' => 	9999, 
			'title' => 		esc_attr__('Masonry', 'theater') 
		), 
		'cmsmasters-full-thumb' => array( 
			'width' => 		1160, 
			'height' => 	675, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Full', 'theater') 
		), 
		'cmsmasters-project-full-thumb' => array( 
			'width' => 		1160, 
			'height' => 	700, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Project Full', 'theater') 
		), 
		'cmsmasters-full-masonry-thumb' => array( 
			'width' => 		1160, 
			'height' => 	9999, 
			'title' => 		esc_attr__('Masonry Full', 'theater') 
		) 
	);
	
	
	return $list;
}

}



/* Project Post Type Registration Rename */
if (!function_exists('theater_project_labels')) {

function theater_project_labels() {
	return array( 
		'name' => 					esc_html__('Projects', 'theater'), 
		'singular_name' => 			esc_html__('Project', 'theater'), 
		'menu_name' => 				esc_html__('Projects', 'theater'), 
		'all_items' => 				esc_html__('All Projects', 'theater'), 
		'add_new' => 				esc_html__('Add New', 'theater'), 
		'add_new_item' => 			esc_html__('Add New Project', 'theater'), 
		'edit_item' => 				esc_html__('Edit Project', 'theater'), 
		'new_item' => 				esc_html__('New Project', 'theater'), 
		'view_item' => 				esc_html__('View Project', 'theater'), 
		'search_items' => 			esc_html__('Search Projects', 'theater'), 
		'not_found' => 				esc_html__('No projects found', 'theater'), 
		'not_found_in_trash' => 	esc_html__('No projects found in Trash', 'theater') 
	);
}

}

// add_filter('cmsmasters_project_labels_filter', 'theater_project_labels');


if (!function_exists('theater_pj_categs_labels')) {

function theater_pj_categs_labels() {
	return array( 
		'name' => 					esc_html__('Project Categories', 'theater'), 
		'singular_name' => 			esc_html__('Project Category', 'theater') 
	);
}

}

// add_filter('cmsmasters_pj_categs_labels_filter', 'theater_pj_categs_labels');


if (!function_exists('theater_pj_tags_labels')) {

function theater_pj_tags_labels() {
	return array( 
		'name' => 					esc_html__('Project Tags', 'theater'), 
		'singular_name' => 			esc_html__('Project Tag', 'theater') 
	);
}

}

// add_filter('cmsmasters_pj_tags_labels_filter', 'theater_pj_tags_labels');



/* Profile Post Type Registration Rename */
if (!function_exists('theater_profile_labels')) {

function theater_profile_labels() {
	return array( 
		'name' => 					esc_html__('Profiles', 'theater'), 
		'singular_name' => 			esc_html__('Profiles', 'theater'), 
		'menu_name' => 				esc_html__('Profiles', 'theater'), 
		'all_items' => 				esc_html__('All Profiles', 'theater'), 
		'add_new' => 				esc_html__('Add New', 'theater'), 
		'add_new_item' => 			esc_html__('Add New Profile', 'theater'), 
		'edit_item' => 				esc_html__('Edit Profile', 'theater'), 
		'new_item' => 				esc_html__('New Profile', 'theater'), 
		'view_item' => 				esc_html__('View Profile', 'theater'), 
		'search_items' => 			esc_html__('Search Profiles', 'theater'), 
		'not_found' => 				esc_html__('No Profiles found', 'theater'), 
		'not_found_in_trash' => 	esc_html__('No Profiles found in Trash', 'theater') 
	);
}

}

// add_filter('cmsmasters_profile_labels_filter', 'theater_profile_labels');


if (!function_exists('theater_pl_categs_labels')) {

function theater_pl_categs_labels() {
	return array( 
		'name' => 					esc_html__('Profile Categories', 'theater'), 
		'singular_name' => 			esc_html__('Profile Category', 'theater') 
	);
}

}

// add_filter('cmsmasters_pl_categs_labels_filter', 'theater_pl_categs_labels');

