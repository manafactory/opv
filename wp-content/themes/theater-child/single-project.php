<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater
 * @version		1.0.5
 * 
 * Single Project Template
 * Created by CMSMasters
 * 
 */


get_header();


list($cmsmasters_layout) = theater_theme_page_layout_scheme();


echo '<!-- Start Content -->' . "\n";


if ($cmsmasters_layout == 'r_sidebar') {
	echo '<div class="content entry">' . "\n\t";
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo '<div class="content entry fr">' . "\n\t";
} else {
	echo '<div class="middle_content entry">';
}


if (have_posts()) : the_post();
	echo '<div class="portfolio opened-article">';
	
	get_template_part('theme-framework/theme-style' . CMSMASTERS_THEME_STYLE . '/postType/portfolio/project-single');
	
	echo '</div>';
endif;


echo '</div>' . "\n" . 
'<!-- Finish Content -->' . "\n\n";


if ($cmsmasters_layout == 'r_sidebar') {
	echo "\n" . '<!-- Start Sidebar -->' . "\n" . 
	'<div class="sidebar">' . "\n";
	
	
	get_sidebar();
	
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- Finish Sidebar -->' . "\n";
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo "\n" . '<!-- Start Sidebar -->' . "\n" . 
	'<div class="sidebar fl">' . "\n";
	
	
	get_sidebar();
	
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- Finish Sidebar -->' . "\n";
}

?>
<script>
//Does a switch of the play/pause with one button.
function playPause(id){
	var song = 'brano'+id;
	var button = 'button'+id;
  //Sets the active song since one of the functions could be play.
  activeSong = document.getElementById(song);

  //Checks to see if the song is paused, if it is, play it from where it left off otherwise pause it.
  if (activeSong.paused){
    activeSong.play();
  }else{
    activeSong.pause();
  }
  
  var element = document.getElementById(button);

	if (element.classList) { 
	  element.classList.toggle("play");
	} else {
	  // For IE9
	  var classes = element.className.split(" ");
	  var i = classes.indexOf("play");
	
	  if (i >= 0) 
		classes.splice(i, 1);
	  else 
		classes.push("play");
		element.className = classes.join(" "); 
	}

}

</script>
<?php

get_footer();
