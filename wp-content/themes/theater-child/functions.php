<?php
/**
 * @package 	WordPress
 * @subpackage 	Theater Child
 * @version		1.0.0
 * 
 * Child Theme Functions File
 * Created by CMSMasters
 * 
 */


function theater_enqueue_styles() {
    wp_enqueue_style('theater-child-style', get_stylesheet_uri(), array(), '1.0.0', 'screen, print');
	wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
}

add_action('wp_enqueue_scripts', 'theater_enqueue_styles', 11);



function caricaBrani($atts){
	$brani = get_field('brani');
	
	$riga = '<div class="brani">';
	
	// check if the repeater field has rows of data
	if( have_rows('brani') ):
	
		$b = 0;
		// loop through the rows of data
		while ( have_rows('brani') ) : the_row();
			$b = $b+1;
			// display a sub field value
			$brano = get_sub_field('brano');
			
			$url = $brano['url'];
			$title = $brano['title'];
			
			$riga .= '<audio id="brano'.$b.'" src="'.$url.'"></audio>';
			$riga .= '<p class="brano">'.$title.' <button class="playpause" id="button'.$b.'" onclick="playPause(\''.$b.'\')"></button></p>';


		endwhile;
	
	endif;
		
	$riga .= '</div>';
	
	return $riga;

}

add_shortcode("brani", "caricaBrani");



?>